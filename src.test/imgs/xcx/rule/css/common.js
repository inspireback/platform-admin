//"use strict";
var H5_AJAX_HTTP_URL = "https://api.cloudokids.cn/API/";
var H5_URL = "https://api.cloudokids.cn/API/";
var H5_AJAX_HTTP_URL_TEST = "http://123.57.43.208:8086/API/";
// var H5_AJAX_HTTP_URL = "http://123.57.43.208:8086/API/";
// var H5_AJAX_HTTP_URL_NEW = "http://123.57.43.208:8089/";
function newAlert(news) {
  var content = '<div class="newBodyBg"></div><div class="newAlert"><p>' + news + '</p></div>';
  $('body').append(content);
  $(".newBodyBg").css("height", $(window).height());
  setTimeout(function() {
    $(".newBodyBg").remove();
    $(".newAlert").remove();
  },
  1500)
}

function galert(news) {
  var newContent = '<div class="gBodyBg"></div><div class="galert"><p>' + news + '</p><div class="gbtn"><button id="confirm">确认</button><button id="cancel">取消</button></div></div>';
  var jx_val = $("#confirm").text();
  if (jx_val != null) {
    $(".gBodyBg").remove();
    $('body').append(newContent);
    $(".gBodyBg").css("height", $(window).height());
  } else {
    $('body').append(newContent);
  }
  $("#confirm").click(function() {
    $(".gBodyBg").remove();
    $(".galert").remove();
  });
  $("#cancel").click(function() {
    $(".gBodyBg").remove();
    $(".galert").remove();
  })
}
// http://123.57.43.208:8086/API/
// https://api.cloudokids.cn/API/

//友盟统计
(function() {
  var hm = document.createElement("script");
  hm.src = "https://s22.cnzz.com/z_stat.php?id=1262002511&web_id=1262002511";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();

/**
 * APP_API FUNCTION independent kd
 */
!function(win) {
  window.__appMark = '';
  function setupWebViewJavascriptBridge(callback){
    if (window.WKWebViewJavascriptBridge) { return callback(WKWebViewJavascriptBridge); }
    if (window.WKWVJBCallbacks) { return window.WKWVJBCallbacks.push(callback); }
    window.WKWVJBCallbacks = [callback];
    if( window.webkit) window.webkit.messageHandlers.iOS_Native_InjectJavascript.postMessage(null)
  };
  if(/\iphone/gi.test(navigator.userAgent)){
    setupWebViewJavascriptBridge(function(bridge) {
      bridge.callHandler('APP_MARK',{}, function(response) {
        __appMark = response;  //2 IOS
      })
    });
  }else if( window.android) {
    __appMark = window.android.APP_MARK()  //1 安卓
  }
}(window)

/**!
* tips：public depend
*/

  let kd = {};
  /**navigator UA */
  kd.agent = { 
    isWeApp: function(){
      return window.__wxjs_environment === 'miniprogram';
    },
    isApp: function(){
      return !!__appMark
    },
    isIphoneX: /\iphone/gi.test(navigator.userAgent) && (screen.height == 812 && screen.width == 375),
    isWX: /\MicroMessenger/gi.test(navigator.userAgent),
  };
  kd.urlToJson = function(url) {
    let result = {};
    url.substr(1).replace(/(\w+)=(\w+)/ig,function(a, b, c) {
      result[b] = decodeURIComponent(c);
    });
    return result;
  };
  kd.h5ToWeApp = function(config) {
   
    config === undefined ?  config = {} : config; 
    const hrefArr = document.querySelectorAll('a');
    hrefArr.forEach(function(item, index) {
      $(item).off('click.app');
      $(item).on('click.app',function(e){
        let nowUrl = this.getAttribute('href');
         if(nowUrl && window.__wxjs_environment === 'miniprogram') {
          if(nowUrl.indexOf('detailPages.html') > -1) { // 详情
            e.preventDefault();
            let result = {};
            nowUrl.substr(1).replace(/(\w+)=(\w+)/ig,function(a, b, c) {
              result[b] = decodeURIComponent(c);
            });
            let spuId = result.spuId;
            if(!spuId) return
            wx.miniProgram.navigateTo({
              url: '/pages/detail/detail?spuId=' + spuId
            })
          } else if (nowUrl.indexOf('singItem.html') > -1) { //品牌/分类/专题
            e.preventDefault();
            let urlJson = kd.urlToJson(nowUrl);
            let pageCome = urlJson.pageCome, myId = urlJson.myId, myTitle = urlJson.myTitle;
            if(pageCome==3 && myId) { //品牌
              wx.miniProgram.navigateTo({
                url: '/pages/newStyle/newStyle?specialId=' + myId + '&title=' + myTitle + '&pageCome=3'
              })
            } else if(pageCome==1 && myId) { //分类
              wx.miniProgram.navigateTo({
                url: '/pages/newStyle/newStyle?specialId=' + myId + '&title=' + myTitle + '&pageCome=2'
              })
            } else if(pageCome==0 && myId) { //专题
              let specialTopicId = urlJson.specialTopicId;  
              wx.miniProgram.navigateTo({
                url: '/pages/newStyle/newStyle?specialId=' + myId + '&specialTopicId=' + specialTopicId + '&title=' + myTitle + '&pageCome=1'
              })
            }
          }
        }
      })
    })
  };
  kd.h5ToClientApp = function() {
    $(function(){
      const hrefArr = document.querySelectorAll('a');
      hrefArr.forEach(function(item, index) {
        var nowUrl = item.getAttribute('href');
        if(nowUrl && kd.agent.isApp() ) {
          if(nowUrl.indexOf('detailPages.html') > -1) { // 详情
            var spuId = kd.urlToJson(nowUrl).spuId;
            if(spuId) item.setAttribute('href', "cloudokids://product/" + spuId)
          } else if (nowUrl.indexOf('singItem.html') > -1) { //品牌/分类/专题
            var urlJson = kd.urlToJson(nowUrl);
            var pageCome = urlJson.pageCome, myId = urlJson.myId, myTitle = urlJson.myTitle;
            if(pageCome==3 && myId) { //品牌
              item.setAttribute('href', "cloudokids://designer/" + myId)
            } else if(pageCome==1 && myId) { //分类
              item.setAttribute('href', "cloudokids://category/" + myId)
            } else if(pageCome==0 && myId) { //专题
              var specialTopicId = urlJson.specialTopicId; 
              item.setAttribute('href', 'cloudokids://topics/' + myTitle + '/' + myId + '/' + myTitle + ',' + specialTopicId)
            }
          }
        }
      })
    })
  };
  kd.h5ToApp = function(){
    this.h5ToWeApp();
    this.h5ToClientApp();
  }
  /** 图片尺寸压缩 */
  kd.imgStyle = function(url,height = 750) {
    typeof height === "undefined" && (height = 750)
    if (url) {
      if (url.indexOf('img1.cloudokids') > -1){
        var img = url.split('/');
        img[img.length - 1] = height + '!' + img[img.length - 1];
        url = img.join('/');
      } else if (url.indexOf('img2.cloudokids') > -1){
        url += '?x-oss-process=image/resize,h_' + height;
      }
    }
    return url;
  }
  
