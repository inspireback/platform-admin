;(function($, window, document,undefined) {
    //瀹氫箟鐨勬瀯閫犲嚱鏁�
    var Drag = function(ele, opt) {
        this.$ele = ele,
            this.x = 0,
            this.y = 0,
            this.defaults = {
                parent:'parent',
                randomPosition:true,
                direction:'all',
                handler:false,
                dragStart:function(x,y){},
                dragEnd:function(x,y){},
                dragMove:function(x,y){}
            },
            this.options = $.extend({}, this.defaults, opt)
    }
    //瀹氫箟鏂规硶
    Drag.prototype = {
        run: function() {
            var $this = this;
            var element = this.$ele;
            var randomPosition  = this.options.randomPosition; //浣嶇疆
            var direction = this.options.direction; //鏂瑰悜
            var handler = this.options.handler;
            var parent = this.options.parent;
            var isDown = false; //璁板綍榧犳爣鏄惁鎸変笅
            var fun = this.options; //浣跨敤澶栭儴鍑芥暟
            var X = 0,
                Y = 0,
                moveX,
                moveY;
            // 闃绘鍐掓场
            element.find('*').not('img').mousedown(function(e) {
                e.stopPropagation();
            });
            //鍒濆鍖栧垽鏂�
            if(parent == 'parent'){
                parent = element.parent();
            }else{
                parent = element.parents(parent);
            }
            if(!handler){
                handler = element;
            }else{
                handler = element.find(handler);
            }
            //鍒濆鍖�
            parent.css({position:'relative'});
            element.css({position:'absolute'});
            var boxWidth=0,boxHeight=0,sonWidth=0,sonHeight=0;
            //鐩掑瓙 鍜� 鍏冪礌澶у皬鍒濆鍖�
            initSize();
            if(randomPosition){randomPlace();}
            $(window).resize(function(){
                initSize();
                if(randomPosition){randomPlace();}
            });
            //鐩掑瓙 鍜� 鍏冪礌澶у皬鍒濆鍖栧嚱鏁�
            function initSize(){
                boxWidth = parent.outerWidth();
                boxHeight = parent.outerHeight();
                sonWidth = element.outerWidth();
                sonHeight = element.outerHeight();
            }
            //浣嶇疆闅忔満鍑芥暟
            function randomPlace(){
                if(randomPosition){
                    var randX = parseInt(Math.random()*(boxWidth-sonWidth));
                    var randY = parseInt(Math.random()*(boxHeight-sonHeight));
                    if(direction.toLowerCase() == 'x'){
                        element.css({left:randX});
                    }else if(direction.toLowerCase() == 'y'){
                        element.css({top:randY});
                    }else{
                        element.css({left:randX,top:randY});
                    }
                }
            }
            handler.css({cursor:'move'}).mousedown(function(e){
                isDown = true;
                X = e.pageX;
                Y = e.pageY;
                $this.x = element.position().left;
                $this.y = element.position().top;
                element.addClass('on');
                fun.dragStart(parseInt(element.css('left')),parseInt(element.css('top')));
                return false;
            });
            $(document).mouseup(function(e){fun.dragEnd(parseInt(element.css('left')),parseInt(element.css('top')));element.removeClass('on');isDown = false;});
            $(document).mousemove(function(e){
                moveX = $this.x+e.pageX-X;
                moveY = $this.y+e.pageY-Y;
                function thisXMove(){ //x杞寸Щ鍔�
                    if(isDown == true){
                        element.css({left:moveX});
                    }else{
                        return;
                    }
                    if(moveX < 0){
                        element.css({left:0});
                    }
                    if(moveX > (boxWidth-sonWidth)){
                        element.css({left:boxWidth-sonWidth});
                    }
                    return moveX;
                }
                function thisYMove(){ //y杞寸Щ鍔�
                    if(isDown == true){
                        element.css({top:moveY});
                    }else{
                        return;
                    }
                    if(moveY < 0){
                        element.css({top:0});
                    }
                    if(moveY > (boxHeight-sonHeight)){
                        element.css({top:boxHeight-sonHeight});
                    }
                    return moveY;
                }
                function thisAllMove(){ //鍏ㄩ儴绉诲姩
                    if(isDown == true){
                        element.css({left:moveX,top:moveY});
                    }else{
                        return;
                    }
                    if(moveX < 0){
                        element.css({left:0});
                    }
                    if(moveX > (boxWidth-sonWidth)){
                        element.css({left:boxWidth-sonWidth});
                    }
                    if(moveY < 0){
                        element.css({top:0});
                    }
                    if(moveY > (boxHeight-sonHeight)){
                        element.css({top:boxHeight-sonHeight});
                    }
                }
                if(isDown){
                    fun.dragMove(parseInt(element.css('left')),parseInt(element.css('top')));
                }else{
                    return false;
                }
                if(direction.toLowerCase() == "x"){
                    thisXMove();
                }else if(direction.toLowerCase() == "y"){
                    thisYMove();
                }else{
                    thisAllMove();
                }
            });
        }
    }

    //鎻掍欢
    $.fn.myDrag = function(options) {
        //鍒涘缓瀹炰綋
        var drag = new Drag(this, options);
        //璋冪敤鏂规硶
        drag.run();
        return this;
    }
})(jQuery, window, document);