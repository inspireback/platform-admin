(function ($) {
    $.fn.extendPagination = function (options) {
        var defaults = {
            totalCount: '',// 总数
            page: 1,// 默认选中的页码，当前页
            showPage: '10', //显示页码数量
            limit: '5',// 每页显示数量
            callback: function () {
                return false;
            }
        };
        $.extend(defaults, options || {});
        if (defaults.totalCount == '') {
            $(this).empty();
            return false;
        } else if (Number(defaults.totalCount) <= 0) {
            $(this).empty();
            return false;
        }
        if (defaults.showPage == '') {
            defaults.showPage = '10';
        } else if (Number(defaults.showPage) <= 0) defaults.showPage = '10';
        if (defaults.limit == '') {
            defaults.limit = '5';
        } else if (Number(defaults.limit) <= 0) defaults.limit = '5';
        var totalCount = Number(defaults.totalCount), showPage = Number(defaults.showPage),
            limit = Number(defaults.limit), totalPage = Math.ceil(totalCount / limit);
        if (totalPage > 0) {
            var html = [];
            html.push(' <ul class="pagination">');
            html.push(' <li class="previous page-item"><a  class="page-link" href="javascript:void(0)">&laquo;</a></li>');
            html.push('<li class=" hidden page-item prev-more"><a  class="page-link" href="javascript:void(0)">...</a></li>');
            if (totalPage <= showPage) {
                for (var i = 1; i <= totalPage; i++) {
                    if ((defaults.page && defaults.page == i) || (!defaults.page && i == 1)) html.push(' <li class="active page-item"><a class="page-link" href="javascript:void(0)">' + i + '</a></li>'); else html.push(' <li class="page-item"><a class="page-link" href="javascript:void(0)">' + i + '</a></li>');
                }
            } else {
                var min = options.page < showPage ? 1 : parseInt(options.page / 2);
                var max = options.page < showPage ? showPage : Math.min(totalPage, (options.page + parseInt(showPage / 2)));
                //Math.min(defaults.page + parseInt((options.page < showPage ? showPage : options.page) / 2), totalPage);
                for (var j = min; j <= (max ); j++) {
                    if ((defaults.page && defaults.page == j) || (!defaults.page && j == 1)) html.push(' <li class="active page-item"><a class="page-link" href="javascript:void(0)">' + j + '</a></li>'); else html.push(' <li class="page-item"><a class="page-link" href="javascript:void(0)">' + j + '</a></li>');
                }
            }
            html.push('<li class=" hidden page-item next-more"><a href="javascript:void(0)" class="page-link ">...</a></li>');
            html.push('<li class="next page-item"><a  class="page-link" href="javascript:void(0)">&raquo;</a></li></ul>');
            $(this).html(html.join(''));
            if (totalPage > showPage && ( totalPage - options.page) > 3) {
                $('.next-more').removeClass('hidden')
            }
            if (totalPage > showPage && (  options.page) > 3) {
                $('.prev-more').removeClass('hidden')
            }
            if (totalPage == defaults.page) {
                $(this).find('ul.pagination li.next').addClass('disabled');
            } else {
                $(this).find('ul.pagination li.next').removeClass('disabled');
            }
            if (1 == defaults.page) {
                $(this).find('ul.pagination li.previous').addClass('disabled');
            } else {
                $(this).find('ul.pagination li.previous').removeClass('disabled');
            }
            var pageObj = $(this).find('ul.pagination'), preObj = pageObj.find('li.previous'),
                currentObj = pageObj.find('li').not('.previous,.disabled,.next'), nextObj = pageObj.find('li.next');

            function loopPageElement(minPage, maxPage) {
                var tempObj = preObj.next();
                for (var i = minPage; i <= maxPage; i++) {
                    if (minPage == 1 && (preObj.next().attr('class').indexOf('hidden')) < 0)
                        preObj.next().addClass('hidden'); else if (minPage > 1 && (preObj.next().attr('class').indexOf('hidden')) > 0)
                        preObj.next().removeClass('hidden');
                    if (maxPage == totalPage && (nextObj.prev().attr('class').indexOf('hidden')) < 0)
                        nextObj.prev().addClass('hidden'); else if (maxPage < totalPage && (nextObj.prev().attr('class').indexOf('hidden')) > 0)
                        nextObj.prev().removeClass('hidden');
                    var obj = tempObj.next().find('a');
                    if (!isNaN(obj.html())) obj.html(i);
                    tempObj = tempObj.next();
                }
            }

            function callBack(curr) {
                defaults.callback(curr, defaults.limit, totalCount);
            }

            currentObj.click(function (event) {
                event.preventDefault();
                ;
                var currPage = Number($(this).find('a').html()), activeObj = pageObj.find('li.active'),
                    activePage = Number(activeObj.find('a').html());
                var dot = false;
                if ((currPage + '') == 'NaN') {
                    dot = true;
                    if ($(this).index() > pageObj.find('li.active').index()) {
                        currPage = Number($(this).prev().find('a').html());
                    }
                    if ($(this).index() < pageObj.find('li.active').index()) {
                        currPage = Number($(this).next().find('a').html());
                    }
                }
                if (currPage == activePage)return false;
                if (totalPage > showPage && currPage > 1) {
                    var maxPage = currPage, minPage = 1;
                    if (($(this).prev().attr('class')) && ($(this).prev().attr('class').indexOf('disabled')) >= 0) {
                        minPage = currPage - 1;
                        maxPage = minPage + showPage - 1;
                        loopPageElement(minPage, maxPage);
                    } else if (($(this).next().attr('class')) && ($(this).next().attr('class').indexOf('disabled')) >= 0) {
                        if (totalPage - currPage >= 1) maxPage = currPage + 1; else maxPage = totalPage;
                        if (maxPage - showPage > 0) minPage = (maxPage - showPage) + 1;
                        loopPageElement(minPage, maxPage)
                    }
                }
                activeObj.removeClass('active');
                $.each(currentObj, function (index, thiz) {
                    if ($(thiz).find('a').html() == currPage) {
                        $(thiz).addClass('active');
                        callBack(currPage);
                    }
                });
            });
            preObj.click(function (event) {
                event.preventDefault();
                var activeObj = pageObj.find('li.active'), activePage = Number(activeObj.find('a').html());
                if (activePage <= 1)return false;
                if (totalPage > showPage) {
                    var maxPage = activePage, minPage = 1;
                    if ((activeObj.prev().prev().attr('class')) && (activeObj.prev().prev().attr('class').indexOf('disabled')) >= 0) {
                        minPage = activePage - 1;
                        if (minPage > 1) minPage = minPage - 1;
                        maxPage = minPage + showPage - 1;
                        loopPageElement(minPage, maxPage);
                    }
                }
                $.each(currentObj, function (index, thiz) {
                    if ($(thiz).find('a').html() == (activePage - 1)) {
                        activeObj.removeClass('active');
                        $(thiz).addClass('active');
                        callBack(activePage - 1);
                    }
                });
            });
            nextObj.click(function (event) {
                event.preventDefault();
                var activeObj = pageObj.find('li.active'), activePage = Number(activeObj.find('a').html());
                if (activePage >= totalPage)return false;
                if (totalPage > showPage) {
                    var maxPage = activePage, minPage = 1;
                    //if ((activeObj.next().next().attr('class')) && (activeObj.next().next().attr('class').indexOf('disabled')) >= 0) {
                    maxPage = activePage + 2;
                    if (maxPage > totalPage) maxPage = totalPage;
                    minPage = maxPage - showPage + 1;
                    loopPageElement(minPage, maxPage);
                }
                //}
                $.each(currentObj, function (index, thiz) {
                    if ($(thiz).find('a').html() == (activePage + 1)) {
                        activeObj.removeClass('active');
                        $(thiz).addClass('active');
                        callBack(activePage + 1);
                    }
                });
            });
        }
    };
})(jQuery);