/**
 * Created by suweiming on 2018/11/28.
 */
$(function () {
    var searchObj = parseSearch();
    $.get('/tpl/detail/' + searchObj.id, function (res) {
        $('#content').html(res.data.remark);
        $('body').css({
            paddingTop:0,
        })
    })
})