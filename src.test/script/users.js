$(function () {
    var query = {}, fields = 'id|userName|password|name|gender|phone',
        $search = $('.search'), $searchBar = $('.search-btn');
    var config = {
        coloum_num: 8, //列数目
        query: '/user/query?',//分页查询URL,
        create: '/user/create',//create,
        update: '/user/update',
        'delete': '/user/delete',
    };
    $searchBar.click(function () {
        $(this).button('loading');
        console.log({keyword: $search.val()});
        search({keyword: $search.val()});

    });
    function search(obj) {
        var query_ = {};
        $.extend(query_, query, obj instanceof Object ? obj : {});
        $loading.fadeIn();
        var arg = [];
        if (!_.isEmpty(query_) && JSON.stringify(query_) == JSON.stringify(query)) { //查询条件和上次一样，取消请求
            $searchBar.button('reset');
            return $loading.fadeOut();
        }
        Object.keys(query_).map(function (key) {
            arg.push(key + '=' + query_[key])
        });
        $.get(config.query + arg.join('&'), function (res) {
            $searchBar.button('reset');
            if (res.list.length) {
                render(res.list)
            } else {
                render(noData(config.coloum_num));
                $('.reset-query').click(function () {
                    $search.val('');
                    $searchBar.click();
                })
            }
            var page = res.page;
            if (_.isEmpty(query)) {
                $('.su-pager').pagination({
                    itemsCount: page.total,
                    pageSize: page.size,
                    styleClass: ['pagination-large'],
                    showCtrl: true,
                    displayPage: 6,
                    onSelect: function (num) {
                        search({page: num});
                        return false;
                    }
                });
                if (!$('.size-select').length) {
                    $('.su-pager').prepend(generatePageSize());
                    $('.size-select').change(function (item) {

                        var size = $(item.target).val();
                        search({size: size, page: 0})
                    })
                }

            }

        });
    }

    function render(list) {
        // $('.table-responsive .table tbody')

        $('.table-responsive .table tbody').html('');
        if (list instanceof Array) {
            list.forEach(function (item, index) {
                // re
                $('.table-responsive .table tbody')
                    .append('<tr> \
                    <td scope="row">' + (index + 1) + ' \
                </td> \
                <td>' + item.name + '</td> \
                <td>' + (item.gender ? '男' : '女') + '</td> \
                <td>' + item.jobNum + '</td> \
                <td>' + item.phone + '</td> \
                <td>' + item.comName + '</td> \
                <td>' + item.depName + '</td> \
            <td class="text-right"> \
                <div class="btn-group" role="group" aria-label="..." style="width: 113px;"> \
                <button type="button" class="btn btn-xs danger btn-default interactive opt-update" data-json=\'' + JSON.stringify(item) + '\' data-toggle="modal" data-target=" .add-user-modal-lg">修改 \
                </button> \
                <button type="button" class="btn btn-xs danger btn-danger opt-delete"   data-loading-text="删除.." data-json=\'' + JSON.stringify(item) + '\'>删除</button> \
                <button type="button" class="btn btn-xs btn-default opt-detail">更多 \
                </button> \
                </div> \
                </td> \
                </tr>')
            });
            $('.opt-update').click(function () {
                $(this).closest('tr').addClass('active').siblings('tr').removeClass('active');
                var item = $(this).data('json');
                fields.split('|').map(function (key) {
                    var $item = $('[name=' + key + ']');
                    if ($item[0].type == 'radio') {
                        $('[name=' + key + '][value=' + item[key] + ']').attr('checked', true)
                    } else {
                        $item.val(item[key]);
                    }
                });
            });
            $('.opt-delete').click(function () {
                $(this).closest('tr').addClass('active').siblings('tr').removeClass('active');
                var self = this;
                $(this).button('loading');

                var item = $(this).data('json');
                if (confirm('确认删除？')) {
                    $.post(config['delete'], {id: item.id}, function (xhr, data) {
                        $(self).button('reset');
                        $(self).closest('tr').remove()
                    });
                } else {
                    $(self).button('reset');
                }
            });
        } else {
            $('.table-responsive .table tbody').html(list);
        }
        $loading.fadeOut();
    }

    search();

    // 表单提交
    var $submit = $('#submit');
    $submit.on('click', function () {
        var $btn = $(this).button('loading');
    });

    $("#user-form").validate({
        submitHandler: function (form_) {
            var form = {};
            fields.split('|').map(function (item) {
                form[item] = form_[item].value
            });
            var url = config.create;
            if (form.id) { // is update?
                url = config.update;
            }
            $.post(url, form, function (xhr, data) {
                $submit.button('reset');
            });
            return false;
        }
    });
    $('.table th .fa').click(function (e) {
        var sort = {sortField: '', sortOrder: ''};
        sort.sortField = $(this).data('sort');
        if ($(this).hasClass('fa-sort') || $(this).hasClass('fa-sort-desc')) {
            sort.sortOrder = 'ASC';
            $(this)
                .removeClass('fa-sort-desc fa-sort')
                .addClass('fa-sort-asc');
            $(this)
                .closest('th')
                .siblings('th')
                .find('.fa')
                .removeClass('fa-sort-desc fa-sort-asc')
                .addClass('fa-sort');
        } else if ($(this).hasClass('fa-sort-asc')) { //asc
            sort.sortOrder = 'DESC';
            $(this)
                .removeClass('fa-sort-asc fa-sort')
                .addClass('fa-sort-desc');
            $(this)
                .closest('th')
                .siblings('th')
                .find('.fa')
                .removeClass('fa-sort-desc fa-sort-asc')
                .addClass('fa-sort')
        }
        search(sort);
    })
});