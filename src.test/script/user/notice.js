$(function () {
    var query = {};
    var config = {
        query: '/notice/query/1/$s?',//分页查询URL,
        create: '/com/create',//create,
        update: '/com/update',
        'delete': '/com/delete'
    };

    function search(obj) {
        var query_ = {
        };
        if($('[name=content]').length){
            query_.content = $('[name=content]').val();
        }
        $.extend(query_, query, obj instanceof Object ? obj : {});
        $loading.fadeIn();
        if (isSameQueryStr(query_, query)) { //查询条件和上次一样，取消请求
            return $loading.fadeOut();
        }
        query = query_;
        query.timestamp = Date.now();
        $.get(config.query.replace('$s',$('[name=read_status]').length?$('[name=read_status]').val():2) + ajaxArgStrGenerator(query), function (res) {
            var _$table = $('.table-responsive .table');
            destroyBtn();
            _$table.$table($.extend(TABLE_SETTING, {
                showSearchComplex: true,
                checkbox: false, // 是否显示复选框
                seqNum: true, // 是否显示序号
                fixedHeader: false,
                sort: search, //排序回调  函数
                destroyBtn: destroyBtn, //禁用按钮
                createBtn: createBtn, // 启用她扭
                toolbar: '.toolbar',
                enableSetting: true,//允许自定义显示列
                search: function (obj) { //搜索框查询函数
                    search(obj)
                },
                rendered: function () {
                    onClick($('.read'), function () {
                        var self = this;
                        $.get('/notice/read/' + $(this).data('id'), function (res) {
                            return window.parent.open( $(self).data('href'),'_blank');
                        })
                    })
                },
                fields: [
                    {
                        enableFilter: true,
                        filters: [
                            {label: '全部', value: '2'},
                            {label: '未读', value: 0},
                            {label: '已读',value: 1}
                        ],
                        style:{width:'60px'},
                        name: 'read_status', label: '状态', filter: function (item) {
                        return '[<a hre="javascript:void(0);" style="cursor: pointer;">' + (item.readStatus == 0 ? '未读' : '已读') + '</a>]';
                    }
                    },
                    {
                        name: 'content', enableSearch:true,label: '内容', filter: function (item) {
                        if (item.url) {
                            return '<a data-id="' + item.id + '" target="_blank" class="read" ' +
                                'href="javascript:void(0);" data-href="'+item.url+'" >' + item.content + '</a>'
                        } else {
                            return item.content;
                        }
                    }
                    },
                    {
                        name: 'createAt', label: '时间', style: 'width:120px;', filter: function (item) {
                        return formatTimeStrWithoutYear(item.createAt)
                    }
                    },
                ],
                data: res.list || [],
            }));

            var page = res.page;
            pageInit(page, query, search);
        });
    }

    search();
});