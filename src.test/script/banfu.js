/**
 * 此文件为班夫系统业务代码
 * Created by suweiming on 2018/1/18.
 */

/**
 * 前端注册账户类型
 * @param type
 * @returns {*}
 */
function banfuUserType(type) {
    if (type == 0)return '竞拍团队';
    if (type == 1)return '竞拍作者';
    if (type == 2)return '导师团队';
}

/**
 *  分类
 * @param t
 * @returns {*}
 */
function banfuCategory(t) {
    var t = {
        info: {label: '分类'}
    }
    return t[type];
}
/**
 * 设置按钮
 */
function setBtn(btns) {
    if (typeof btns == 'array') {
        btns.map(function (item) {
            $('.toolbar').append(item);
        })
    } else {
        [btns].map(function (item) {
            $('.toolbar').append(item);
        })
    }

}
