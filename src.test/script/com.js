$(function () {
    var query = {};

    function search(obj) {

        query = $.extend(query, obj instanceof Object ? obj : {});

        $.get('/user/query', function (res) {
            console.log(res);
            render([])
        })
    }

    function render(list) {
        // $('.table-responsive .table tbody')

        (function (item, index) {
            // re
            $('.table-responsive .table tbody')
                .append('<tr> \
                    <td scope="row">1 \
                </td> \
                <td>实战者团队</td> \
                <td>029-88293659</td> \
                <td>13395-15612</td> \
                <td>陕西省西安市丈八北路派出所110号</td> \
                <td>招商银行</td> \
                <td>6225 7687 2705 6409</td> \
            <td>150IDC7891</td> \
            <td>单位明细</td> \
            <td class="text-right"> \
                <div class="btn-group" role="group" aria-label="..." style="width: 113px;"> \
                <button type="button" class="btn btn-xs danger btn-default interactive opt-update" data-toggle="modal" data-target=" .add-user-modal-lg">修改 \
                </button> \
                <button type="button" class="btn btn-xs danger btn-danger opt-delete">删除</button> \
                <button type="button" class="btn btn-xs btn-default opt-detail">更多 \
                </button> \
                </div> \
                </td> \
                </tr>')
        })
    }

    $('.su-pager').pagination({
        pages: 50,
        styleClass: ['pagination-large'],
        showCtrl: true,
        displayPage: 6,
        onSelect: function (num) {
            console.log(num); //打开控制台观察
            query['page'] = num;
            return false;
        }
    });
    // 表单提交
    var $submit = $('#submit');
    $submit.on('click', function () {
        var $btn = $(this).button('loading');
    });
    $('#com-form').submit(function (e) {
        var fields = 'name|tel|fax|address|bank|bankAccount|taxNum';
        e.preventDefault();
        var form = {};
        fields.split('|').map(function (item) {
            form[item] = e.target[item].value
        });
        console.log(form);
        $submit.button('reset');
        $.post('/admin/com/add', form, function (data) {
            console.log(data);
        });
        return false;
    });

    $('.table th .fa').click(function (e) {
        var sort = {field: '', sort: ''};
        sort.field = $(this).data('sort');
        if ($(this).hasClass('fa-sort') || $(this).hasClass('fa-sort-desc')) {

            sort.sort = 'asc';
            $(this)
                .removeClass('fa-sort-desc fa-sort')
                .addClass('fa-sort-asc');
            $(this)
                .closest('th')
                .siblings('th')
                .find('.fa')
                .removeClass('fa-sort-desc fa-sort-asc')
                .addClass('fa-sort');
        } else if ($(this).hasClass('fa-sort-asc')) { //asc
            sort.sort = 'desc';
            $(this)
                .removeClass('fa-sort-asc fa-sort')
                .addClass('fa-sort-desc');
            $(this)
                .closest('th')
                .siblings('th')
                .find('.fa')
                .removeClass('fa-sort-desc fa-sort-asc')
                .addClass('fa-sort')
        }
        console.log(sort);
    })
});