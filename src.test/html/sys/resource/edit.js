$(function () {
    var content_ = "";
    var $search = parseSearch(),
        data = {
            device: {
                detail: '/device/detail/' + $search.id,
                add: '/device/add',
                update: '/device/update',
                data: [
                    {
                        type: 'text',
                        label: '设备名称',
                        require: true, // 是否必填
                        placeholder: '请输入设备名称',
                        name: 'name',
                        value: '1',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'select',
                        label: '设备分类',
                        require: true, // 是否必填
                        placeholder: '请输入设备描述',
                        name: 'categoryId',
                        value: '',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'editor',
                        label: '设备简介',
                        placeholder: '请输入设备简介',
                        name: 'profile',
                        val: '',
                        num: 8,
                        labelNum: 2,
                        //editorClass:'editor-prifile',
                        editorId: 'editor-profile',
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'text',
                        label: '设备描述',
                        require: true, // 是否必填
                        placeholder: '请输入设备描述',
                        name: 'desc1',
                        value: '1',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'upload-btn',
                        label: '设备图',
                        require: true, // 是否必填
                        name: 'img',
                        value: '',
                        previewClass: 'preview',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                        btn: {
                            class_: '9 col-xs-offset-1'
                        }
                    },
                ],
                reform: function (data) {
                    data.type1 = '0';
                    return data;
                },
                shown: function () {
                    selectGoods({
                        url: '/category/list?type1=device',
                        name: 'categoryId',
                        selected: arg.data[1].val || '',
                        change: function () {
                        }
                    });
                    onClick($('.upload-select'), function () {
                        upload('设备图', {w: 276, h: 320});
                    });
                    initEditor('editor-profile');
                }
            },
            teacher: {
                detail: '/salePerson1/detail/' + $search.id,
                add: '/salePerson1/add',
                update: '/salePerson1/update',
                data: [
                    {
                        type: 'text',
                        label: '导师姓名',
                        require: true, // 是否必填
                        placeholder: '请输入导师姓名',
                        name: 'name',
                        value: '1',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'editor',
                        label: '导师简介',
                        require: true, // 是否必填
                        placeholder: '请输入导师简介',
                        name: 'profile',
                        value: '',
                        num: 8,
                        labelNum: 2,
                        editorId: 'editor-profile',
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'upload-btn',
                        label: '导师头像',
                        require: true, // 是否必填
                        name: 'avatar',
                        value: '',
                        previewClass: 'preview',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                        btn: {
                            ctner: 'form-submit',
                            class_: '9 col-xs-offset-1'
                        }
                    },

                ],
                formatArg: function (data) {
                    data.profile = (data.si && data.si.remark) || '';
                    return data;
                },
                reform: function (data) {
                    data.type1 = '3';
                    data.status = '2'; // 修改、新增默认不自动上线
                    data.avatar = $('.preview').attr('src');
                    return data;
                },
                shown: function () {
                    onClick($('.upload-select'), function () {
                        upload('导师头像', {w: 240, h: 240, style: 'text-align:right;'});
                    });
                    initEditor('editor-profile', arg.data[1].val);
                }
            },
            teacherZuopin: {
                detail: '/info/get?id=' + $search.id,
                add: '/info/create',
                update: '/info/update',
                data: [
                    {
                        type: 'text',
                        label: '作品名称',
                        require: true, // 是否必填
                        placeholder: '作品名称',
                        name: 'title',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'editor',
                        label: '作品简介',
                        require: true, // 是否必填
                        name: 'content',
                        value: '',
                        num: 8,
                        labelNum: 2,
                        editorId: 'editor-zuopin',
                        validate: 'ipt  validate[required]',
                        btn: {
                            class_: '9 col-xs-offset-1'
                        }
                    },
                    {
                        type: 'select',
                        label: '导师',
                        require: true, // 是否必填
                        placeholder: '请选择导师',
                        name: 'ownerId',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'upload-btn',
                        label: '作品缩略图',
                        require: true, // 是否必填
                        name: 'thumbnail',
                        value: '',
                        previewClass: 'preview',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                        btn: {
                            class_: '9 col-xs-offset-1'
                        }
                    },
                ],
                reform: function (data) {
                    data.content = data.editorValue
                    data.thumbnail = $('.preview').attr('src');
                    data.status = 2;//修改后下线
                    data.type1 = 2;
                    return data;
                },
                shown: function () {
                    onClick($('.upload-select'), function () {
                        upload('作品缩略图', {w: 265, h: 380});
                    });
                    selectGoods({
                        url: '/salePerson1/query?type1=3',
                        name: 'ownerId',
                        label: '导师',
                        selected: arg.data[2].val || '',
                        change: function () {
                        }
                    });
                    initEditor('editor-zuopin', arg.data[1].val);

                }
            },
            ads: {
                detail: '/ads/get?id=' + $search.id,
                add: '/ads/create',
                update: '/ads/update',
                data: [
                    {
                        type: 'text',
                        label: '排序号',
                        require: true, // 是否必填
                        placeholder: '排序号',
                        name: 'order',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'upload-btn',
                        label: ($search.type1 ? '合作伙伴LOGO' : '推荐图'),
                        require: true, // 是否必填
                        name: 'url',
                        previewClass: 'preview',
                        value: '',
                        num: 8,
                        labelNum: 2,
                        editorId: 'editor-zuopin',
                        validate: 'ipt  validate[required]',

                    },
                    {
                        type: 'text',
                        label: '链接',
                        require: false, // 是否必填
                        placeholder: '请填写链接',
                        name: 'link',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                        btn: {
                            class_: '9 col-xs-offset-1'
                        }
                    },
                ],
                reform: function (data) {
                    data.url = $('.preview').attr('src');
                    data.status = 2;//修改后下线
                    data.type1 = $search.type1;//数据类型
                    //data.type1 = 2;
                    $search.type = $search.type1 ? 'partner' : $search.type;
                    return data;
                },
                shown: function () {
                    onClick($('.upload-select'), function () {
                        upload($search.type1 ? '合作伙伴LOGO' : '广告图', $search.type1 ? {w: 150, h: 150} : {w: 260, h: 174});
                    });

                }
            },
            tpl: {
                detail: '/tpl/detail/' + $search.id,
                add: '/tpl/add',
                update: '/tpl/update',
                data: [
                    {
                        type: 'text',
                        label: '模板名称',
                        require: true, // 是否必填
                        placeholder: '请输入模板名称',
                        name: 'name',
                        value: '',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'select',
                        label: '模板类型',
                        require: true, // 是否必填
                        placeholder: '请选择模板',
                        name: 'type1',
                        value: '',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'codemirror',
                        label: '模板代码',
                        require: true, // 是否必填
                        placeholder: '模板编辑',
                        name: 'content',
                        num: 8,
                        labelNum: 2,
                        editorId: 'editor-tpl',
                        validate: 'ipt  validate[required]',
                        btn: {
                            class_: '9 col-xs-offset-1',
                            ctner:'form-submit'
                        }
                    },

                ],
                reform: function (data) {
                    data.content = CodeMirrorEditor.getValue();
                    return data;
                },
                formatArg: function (data) {
                    console.log('te');
                    data.content = data.remark;
                    return data;
                },
                shown: function () {
                    initSelect({
                        data: [{id: '1', name: '首页模板'}],
                        name: 'type1',
                        selected: arg.data[1].val || '',
                        change: function () {
                        }
                    });
                    fnCodeMirror({eleId: 'editor-tpl', content: arg.data[2].val}, arg.data[2].val, {source: true});
                }
            },
            frontMenu: {
                detail: '/category/detail/' + $search.id,
                add: '/category/add',
                update: '/category/update',
                data: [
                    {
                        type: 'text',
                        label: '菜单名称',
                        require: true, // 是否必填
                        placeholder: '请输入菜单名称',
                        name: 'name',
                        value: '',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'select',
                        label: '模板类型',
                        require: true, // 是否必填
                        placeholder: '请选择模板',
                        name: 'type1',
                        value: '',
                        num: 8,
                        labelNum: 2,
                        validate: 'ipt  validate[required]',
                    },
                    {
                        type: 'codemirror',
                        label: '模板代码',
                        require: true, // 是否必填
                        placeholder: '模板编辑',
                        name: 'content',
                        num: 8,
                        labelNum: 2,
                        editorId: 'editor-tpl',
                        validate: 'ipt  validate[required]',
                        btn: {
                            class_: '9 col-xs-offset-1',
                            ctner:'form-submit'
                        }
                    },

                ],
                reform: function (data) {
                    data.content = CodeMirrorEditor.getValue();
                    return data;
                },
                formatArg: function (data) {
                    console.log('te');
                    data.content = data.remark;
                    return data;
                },
                shown: function () {
                    initSelect({
                        data: [{id: '1', name: '首页模板'}],
                        name: 'type1',
                        selected: arg.data[1].val || '',
                        change: function () {
                        }
                    });
                    fnCodeMirror({eleId: 'editor-tpl', content: arg.data[2].val}, arg.data[2].val, {source: true});
                }
            },
        }, arg = '';
    arg = data[$search.type];
    if ($search.id) { // 修改
        arg = data[$search.type];
        $.get(arg.detail, function (res) {
            if (res.code == 200) {
                if (arg.formatArg) {
                    res.data = arg.formatArg(res.data);
                }
                arg.data = arg.data.map(function (item) {
                    if (res.data[item.name]) item.val = res.data[item.name];
                    return item;
                })
                genForm();
            }
        })
    } else {
        genForm();
    }
    function upload(lab, ratio) {
        ratio = ratio || {};
        dialogForm({
            title: '上传' + lab,
            fullScreen: true,
            data: [

                {
                    type: 'upload-dialog',
                    label: lab,
                    require: true, // 是否必填
                    name: 'upload',
                    labelStyle: ratio.style || '',
                    value: '',

                    num: 8,
                    labelNum: 2,
                    validate: 'ipt  validate[required]',
                    btnClass: 'select-file',
                    btn: {
                        class_: '9 col-xs-offset-1'
                    }
                },
            ],
            shown: function (obj) {
                $('.select-file').$upload({
                    w: ratio.w || 185,
                    h: ratio.h || 164,
                    submit: function (path) {
                        closeLayer(obj.index);
                        $('.preview').attr('src', path);
                    }
                })
            },
            noForm: true,
            submit: function (data) {
            }
        });
    }

    var self = this;

    function genForm() {
        dialogForm({
            eleId: 'form',
            title: '',
            data: arg.data,
            shown: arg.shown || function () {
            },
            submit: function (data) {
                data.img = $('.preview').attr('src');
                var url = arg.add;
                if ($search.id) {
                    data.id = $search.id;
                    url = arg.update;
                }

                // if (content_) {
                //     data.profile = content_;
                // }
                if (data.editorValue) {
                    data.profile = data.editorValue
                }
                if (arg.reform) {
                    data = arg.reform(data);
                }
                $.post(url, data, function (res) {
                    if (res.code == 200) {
                        fnSuccess('操作成功！');

                        window.location.href = './index.html?type=' + $search.type
                    } else {
                        fnFail(res.error || '操作失败！')
                    }
                });
            }
        }, true);
    }

    /**
     * 富文本编辑器
     * @param id
     */
    function initEditor(id, content, opt) {
        opt = opt || {};
        window.editor = UE.getEditor(id, {
            //工具栏
            toolbars: [[
                'Source',
                'fullscreen', 'undo', 'redo', '|',
                'bold', 'italic', 'underline', 'strikethrough', , 'removeformat', 'formatmatch',
                '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', '|',
                'rowspacingtop', 'rowspacingbottom', 'lineheight', '|', 'link', 'unlink', '|',
                'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|', 'indent', '|',
                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                'insertframe', '|',
                'imagenone', 'imageleft', 'imageright', 'imagecenter', '|', 'simpleupload',
                'horizontal', 'insertvideo'
                // ,'preview'
            ]],
            lang: "zh-cn",//字体
            fontfamily: [
                {label: '', name: 'songti', val: '宋体,SimSun'},
                {label: '', name: 'kaiti', val: '楷体,楷体_GB2312, SimKai'},
                {label: '', name: 'yahei', val: '微软雅黑,Microsoft YaHei'},
                {label: '', name: 'heiti', val: '黑体, SimHei'},
                {label: '', name: 'lishu', val: '隶书, SimLi'},
                {label: '', name: 'andaleMono', val: 'andale mono'},
                {label: '', name: 'arial', val: 'arial, helvetica,sans-serif'},
                {label: '', name: 'arialBlack', val: 'arial black,avant garde'},
                {label: '', name: 'comicSansMs', val: 'comic sans ms'},
                {label: '', name: 'impact', val: 'impact,chicago'},
                {label: '', name: 'timesNewRoman', val: 'times new roman'}
            ],//字号
            'fontsize': [10, 11, 12, 14, 16, 18, 20, 24, 36],
            enableAutoSave: false,
            autoHeightEnabled: false,
            initialFrameHeight: '400px',
            initialFrameWidth: '100%',
            readonly: false,
            initialFrameHeight: 300,
            whitList: {
                a: ['target', 'href', 'title', 'class', 'style'],
                abbr: ['title', 'class', 'style'],
                address: ['class', 'style'],
                area: ['shape', 'coords', 'href', 'alt'],
                article: [],
                aside: [],
                audio: ['autoplay', 'controls', 'loop', 'preload', 'src', 'class', 'style'],
                b: ['class', 'style'],
                bdi: ['dir'],
                bdo: ['dir'],
                big: [],
                blockquote: ['cite', 'class', 'style'],
                br: [],
                caption: ['class', 'style'],
                center: [],
                cite: [],
                code: ['class', 'style'],
                col: ['align', 'valign', 'span', 'width', 'class', 'style'],
                colgroup: ['align', 'valign', 'span', 'width', 'class', 'style'],
                dd: ['class', 'style'],
                del: ['datetime'],
                details: ['open'],
                div: ['class', 'style'],
                dl: ['class', 'style'],
                dt: ['class', 'style'],
                em: ['class', 'style'],
                font: ['color', 'size', 'face'],
                footer: [],
                h1: ['class', 'style'],
                h2: ['class', 'style'],
                h3: ['class', 'style'],
                h4: ['class', 'style'],
                h5: ['class', 'style'],
                h6: ['class', 'style'],
                header: [],
                hr: [],
                i: ['class', 'style'],
                img: ['src', 'alt', 'title', 'width', 'height', 'id', '_src', '_url', 'loadingclass', 'class', 'data-latex'],
                ins: ['datetime'],
                li: ['class', 'style'],
                mark: [],
                nav: [],
                ol: ['class', 'style'],
                p: ['class', 'style'],
                pre: ['class', 'style'],
                s: [],
                section: [],
                small: [],
                span: ['class', 'style'],
                sub: ['class', 'style'],
                sup: ['class', 'style'],
                strong: ['class', 'style'],
                table: ['width', 'border', 'align', 'valign', 'class', 'style'],
                tbody: ['align', 'valign', 'class', 'style'],
                td: ['width', 'rowspan', 'colspan', 'align', 'valign', 'class', 'style'],
                tfoot: ['align', 'valign', 'class', 'style'],
                th: ['width', 'rowspan', 'colspan', 'align', 'valign', 'class', 'style'],
                thead: ['align', 'valign', 'class', 'style'],
                tr: ['rowspan', 'align', 'valign', 'class', 'style'],
                tt: [],
                u: [],
                ul: ['class', 'style'],
                video: ['autoplay', 'controls', 'loop', 'preload', 'src', 'height', 'width', 'class', 'style'],
                source: ['src', 'type'],
                // embed:['type','class','pluginspage','src','wdith','height','align','style','wmode','play','autoplay',
                //     'loop','menu','allowsscriptaccess','allowfullscreen','controls','preload'
                // ],
                iframe: ['src', 'class', 'height', 'width', 'max-width', 'max-height', 'align',
                    'frameborder', 'allowfullscreen'
                ]
            }
        });
        window.editor.ready(function (ueditor) {

            if (opt && opt.source) { // 源码模式
                setTimeout(function () {
                    if (window.editor.queryCommandState('source') != 1) {//判断编辑模式状态:0表示【源代码】HTML视图；1是【设计】视图,即可见即所得；-1表示不可用
                        window.editor.execCommand('source'); //切换到【设计】视图
                    }
                }, 500);
            }
            if (content) {
                window.editor.setContent(content);
            }

        });

        window.editor.addListener('contentChange', function (editor_) {
            var self = this;
            content_ = self.getContent();
        });

    }

    /**
     * 代码编辑器
     * @param opt
     */
    function fnCodeMirror(opt) {
        var myTextarea = document.getElementById(opt.eleId);
        window.CodeMirrorEditor = CodeMirror.fromTextArea(myTextarea, {
            mode: "text/javascript",
            theme: "dracula",	//设置主题
            lineNumbers: true,	//显示行号
            lineWrapping: true,	//代码折叠
            //foldGutter: true,
            //fullScreen: true,
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
            viewportMargin:Infinity,
            matchBrackets: true,
        });
        if (opt.content) {
            window.CodeMirrorEditor.setValue(opt.content)
        }
    }

});