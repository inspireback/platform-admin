$(function () {
    var $search = parseSearch(), arg = {}, tag = '';
    console.log($search);
    function operRes(res) {
        if (res.code == 200) {
            fnSuccess('操作成功');
            search({}, true);
        } else {
            fnFail(res.error || '操作失败');
        }
    }

    var diff = {
        image: {
            query: '/category/list?type1=folder',//分页查询URL,
            tag: '分类',
            listFilter: function (list) {
                $('.cards-list').html('<div class="hp-clearfix"></div>')
                return list;
            },
            btns: [
                {
                    label: '新建文件夹',
                    class_: 'create',
                    onClick: function () {
                        var $index = dialogForm(
                            {
                                title: "新建文件夹",
                                data: [
                                    {
                                        type: 'text',
                                        label: '文件夹名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入文件夹名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '英文名称',
                                        val: '',
                                        require: true, // 是否必填
                                        placeholder: '请输入英文名称',
                                        name: 'en'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: '0',
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.level = 1;
                                    form.type1 = 'folder'; // 数据类型菜单
                                    $.post('/category/add', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '修改文件夹',
                    class_: 'update',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        var $index = dialogForm(
                            {
                                title: "修改文件件",
                                data: [
                                    {
                                        type: 'text',
                                        label: '文件夹名称',
                                        val: $json.name,
                                        require: true, // 是否必填
                                        placeholder: '请输入文件夹名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '英文名称',
                                        val: $json.en,
                                        require: true, // 是否必填
                                        placeholder: '请输入英文名称',
                                        name: 'en'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: $json.orderNum,
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.id = $json.id; // 数据类型菜单
                                    form.type1 = 'folder'; // 数据类型菜单
                                    $.post('/category/update', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '上传',
                    class_: 'upload',
                    onClick: function () {
                        var $json = $(this).data('json');
                        window.location.href = './img.html?id=' + $json.id + '&noCrop=1'

                    }
                },
                {
                    label: '删除',
                    class_: 'delete',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        if (confirm('确认删除？')) {
                            $.post('/category/delete/' + $json.id, {}, function (res) {
                                if (res.code == 200) {
                                    search({}, true);
                                    fnSuccess('删除成功！');
                                }
                            })
                        }
                    }
                }
            ],
            destroyBtn: function () {
                disableBtn(btns[3]);
                disableBtn(btns[2]);
                disableBtn(btns[1]);
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                enableBtn(btns[1], $item);
                enableBtn(btns[2], $item);
                enableBtn(btns[3], $item);
            },
            fields: [
                {name: 'name', label: '名称'},
                {name: 'level', label: '层级'},
                {name: 'en', label: '英文名'},
                {name: 'desc1', label: '描述'},
                {name: 'orderNum', label: '排序号'},
                {
                    name: 'url', label: '封面图', filter: function (item) {
                    if (item.url) {
                        return '<a target="_blank" href="' + item.url + '" title="点击预览">' + item.url + '</a>'
                    } else {
                        return '';
                    }
                }
                },
            ],
        },

    };
    if ($search.type) {
        arg = diff[$search.type];
        if (!arg) {
            return $('.table').html('<p style="text-align: center;line-height: 150px;font-size: 16px;">此功能开发中,敬请期待！</p>');
        }
    }
    var btns = arg.btns.map(function (item) { // 设置分类
        var btn = genBtn(item);
        if (item.onClick) onClick(btn, item.onClick)
        return btn;
    });
    setBtn(btns);

    function search(obj, isForce, isExport) {
        arg.destroyBtn();
        return gSearch({
            table: '.table',
            url: arg.query,
            query: obj,
            isForce: isForce,
            isExport: isExport,
            render: function (res) {
                var _$table = $('.cards-list');
                destroyBtn();
                _$table.$card($.extend(TABLE_SETTING, {
                    checkbox: true, // 是否显示复选框
                    seqNum: true, // 是否显示序号
                    sort: search, //排序回调函数
                    destroyBtn: arg.destroyBtn, //禁用按钮
                    createBtn: arg.createBtn, // 启用她扭
                    toolbar: '.toolbar',
                    enableSetting: true,//允许自定义显示列
                    search: search,
                    showSearchComplex: true,
                    complexItems: [
                        {label: "", name: 'date', dateRange: true},
                    ],
                    rendered: arg.rendered || function () {
                    },
                    fields: arg.fields,
                    data: arg.listFilter ? arg.listFilter(res) : res || [],
                }));
            }
        }, obj.first ? false : true);
    }

    search({}, true);
})

