$(function () {
    var $search = parseSearch(), arg = {}, tag = '';
    console.log($search);
    function operRes(res) {
        if (res.code == 200) {
            fnSuccess('操作成功');
            search({}, true);
        } else {
            fnFail(res.error || '操作失败');
        }

    }

    $('.folder-header').find('span').html($search.name);
    // $('.folder-header').find('strong').html(($search.remark && $search.remark != 'undefined') ? $search.remark : 0);

    upload('文件', {
        w: 100,
        h: 100,
        extraData: {from: 2, folderId: $search.id},
        uploadBtn: true,
        label: '上传至文件夹：' + $search.name,
        class_: '.uploader',
        submitEachTime: function () {
            search({}, true);
        }
    });
    $search.type = 'image';
    var diff = {
        image: {
            query: '/file/list?folderId=' + $search.id,//分页查询URL,
            tag: '分类',
            btns: [
                {
                    label: '<i class="fa fa-cloud-upload"></i> 上传到文件夹',
                    class_: 'upload default btn-su select-file',
                    onClick: function () {
                        var $json = $(this).data('json');

                        // window.location.href = './img.html?id=' + $json.id + '&noCrop=1'
                    }
                },
            ],
            rendered: function () {
                onClick($('.upload'), function () {
                    upload('文件', {w: 100, h: 100});
                })
            },
            listFilter: function (list) {
                $('.cards-list').html('<div class="hp-clearfix"></div>')
                return list;
            },
            destroyBtn: function () {
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
            },
            fields: [
                {}
            ],
        }
    };
    arg = diff.image;
    if (arg.btns) {
        var btns = arg.btns.map(function (item) { // 设置分类
            var btn = genBtn(item);
            if (item.onClick) onClick(btn, item.onClick)
            return btn;
        });
        //setBtn(btns);
    }
    function search(obj, isForce, isExport) {
        arg.destroyBtn && arg.destroyBtn();
        return gSearch({
            table: '.table',
            url: arg.query,
            query: obj,
            isForce: isForce,
            isExport: isExport,
            render: function (res) {
                var _$table = $('.cards-list');
                destroyBtn();
                _$table.$card($.extend(TABLE_SETTING, {
                    checkbox: true, // 是否显示复选框
                    seqNum: true, // 是否显示序号
                    sort: search, //排序回调函数
                    destroyBtn: arg.destroyBtn, //禁用按钮
                    createBtn: arg.createBtn, // 启用她扭
                    toolbar: '.toolbar',
                    enableSetting: true,//允许自定义显示列
                    search: search,
                    isLeaf: true, // 叶子节点
                    showSearchComplex: true,
                    complexItems: [
                        {label: "", name: 'date', dateRange: true},
                    ],
                    delUrl: '/file/delete',
                    rendered: arg.rendered || function () {
                    },
                    fields: arg.fields,
                    data: arg.listFilter ? arg.listFilter(res) : res || [],
                }));
            }
        }, obj.first ? false : true);
    }

    arg.query && search({}, true);
})

