$(function () {
    var query = {}, coms = [],
        fields = 'id|name|tel|fax|address|bank|bankAccount|taxNum',
        $search = $('.search'),
        $searchBar = $('.search-btn'),
        $update = $('.btn-update'),
        $delete = $('.btn-delete'),
        $create = $('.btn-create'),
        $setting = $('.btn-setting'),
        $log = $('.btn-log'),
        strHtml = $('#form-template').html();
    var config = {
        coloum_num: 8, //列数目
        query: '/dep/query?',//分页查询URL,
        create: '/dep/create',//create,
        update: '/dep/update',
        'delete': '/dep/delete',
        com: '/com/query',
    };

    function getComs() {
        var dtd = $.Deferred();
        if (coms.length) {
            dtd.resolve(coms);
        } else {
            $.get(config.com, function (data) {
                if (data.code == 200) {
                    coms = data.list;
                    dtd.resolve(data.list)
                } else {
                    data.reject(data.error);
                }
            });
        }
        return dtd.promise();
    }

    onClick($log, function () {
        var $current = $(this).data('json');
        window.location = './../../log/log.html?id=' + $current.id + '&type=dep';
    });
    function renderModal(title, $current) {

        $.when(getComs())
            .done(function (coms) {
                $current.coms = coms;
                var $modal = layerAlert({
                    title: title,
                    full:true,
                    body: ejs.render(strHtml, $current),
                    shown: function () {
                        validate($modal);
                    },
                });
            })
            .fail(function (e) {
                console.log(e)
            })

    }

    function search(obj, isForce) {
        var query_ = {};
        $.extend(query_, query, obj instanceof Object ? obj : {});
        if (!isForce) {
            if (isSameQueryStr(query_, query)) { //查询条件和上次一样，取消请求
                return $loading.fadeOut();
            }
        }
        query = query_;
        query.timestamp = Date.now();
        $.get(config.query + ajaxArgStrGenerator(query), function (res) {
            var _$table = $('.table-responsive .table');
            destroyBtn();
            _$table.$table($.extend(TABLE_SETTING, {
                checkbox: true, // 是否显示复选框
                seqNum: true, // 是否显示序号
                sort: search, //排序回调函数
                destroyBtn: destroyBtn, //禁用按钮
                createBtn: createBtn, // 启用她扭
                toolbar: '.toolbar',
                enableSetting: true,//允许自定义显示列
                search: function (obj) { //搜索框查询函数
                    search(obj)
                },
                fields: [
                    {name: 'name', _sort: 'name', label: '部门名称', enableSort: true, enableSearch: true},
                    {name: 'comName', _sort: 'comName', label: '所在公司'},
                    {name: 'tel', _sort: 'tel', label: '电话', enableSort: true, enableSearch: true},
                    {name: 'fax', _sort: 'fax', label: '传真'},
                    {
                        name: 'createAt', label: '创建时间', filter: function (item) {
                            return moment(item.createAt).format('YYYY-MM-DD HH:mm:ss')
                        }
                    }
                ],
                data: res.list || [],
            }));
            var page = res.page;
            pageInit(page, query, search);
        });
    }

    $update.unbind('click').click(function () {
        var $current = $(this).data('json');
        renderModal('修改部门"' + $current.name + '"', $current);
    });


    $delete.unbind('click').click(function () {
        var $current = $(this).data('json'), self = this;
        $(self).button('loading');
        if (confirm('确认删除"' + $(this).data('json').name + '"?')) {
            $.post(config['delete'], {id: $current.id}, function (xhr, data) {
                $(self).button('reset');
                if (xhr.code == 200) {
                    fnSuccess('删除成功!');
                    $('tr[id=' + $current.id + ']').remove();
                    // $(self).closest('tr').remove();
                    setTimeout(function () {
                        destroyBtn();
                    }, 0)
                } else {
                    alert('删除失败：' + xhr.error);
                }
            })
        } else {
            $(self).button('reset');
        }
    });
    $create.unbind('click').click(function () {
        renderModal('新建部门', {name: ''});
    });

    function destroyBtn() {
        $update.data('json', null);
        $delete.data('json', null);
        $log.data('json', null);
        disableBtn($update);
        disableBtn($log);
        disableBtn($delete);
    }

    function createBtn(_this) {
        destroyBtn();
        $update.data('json', ($(_this).data('json')));
        $delete.data('json', ($(_this).data('json')));
        $log.data('json', ($(_this).data('json')));
        enableBtn($update);
        enableBtn($log);
        enableBtn($delete);
    }

    search({}, true);
    function validate(modal) {
        $('#registrationForm')
            .formValidation('destroy').formValidation($.extend({}, VALIDATION_SETTING, {
            fields: {
                name: {
                    row: '.col-xs-7',
                    validators: {
                        notEmpty: {
                            message: '部门名称'
                        },
                        stringLength: {
                            min: 2,
                            max: 40,
                            message: '部门名称长度最少3位，最多40位'
                        },
                    }
                },
                comid: {
                    row: '.col-xs-7',
                    validators: {
                        notEmpty: {
                            message: '请选择单位'
                        }
                    }
                },
                tel: {
                    row: '.col-xs-7',
                    validators: {
                        notEmpty: {
                            // message: '必填'
                        },
                        phone: {
                            // country: 'CN',
                            // message:'请输入正确电话'
                        }
                    }
                },
                fax: {
                    row: '.col-xs-7',
                    validators: {}
                },

            }
        }))
            .off('success.form.fv').on('success.form.fv', function (e) {
            e.preventDefault();
            var form = parseSearch($(e.target).serialize());
            var url = '';
            if (form.id) { // is update?
                url = config.update;
            } else {
                url = config.create;
                delete form.id;
            }
            if (form.name) {
                form.name = form.name.trim();
            }
            $.post(url, form, function (xhr, data) {
                if (xhr.code == 200) {
                    layer.closeAll();
                    fnSuccess(form.id ? '保存成功' : '创建成功!');
                    search({}, true);

                    //modal && modal.modal('hide');
                } else {
                    alert(xhr.error || xhr.msg);
                }
            });
        });
    }
});