$(function () {
    var query = {},
        $view = $('.view'),
        $delete = $('.btn-delete'),
        $update = $('.btn-update'),
        $create = $('.btn-create'),
        $updatePermission = $('.btn-update-permission'),
        $log = $('.btn-log');
    var bars = getToolBtns();
    var config = {
        query: '/role/queryList?',//分页查询URL,
        create: '/role/create',//create,
        update: '/role/update',
        getPermission: '/role/getPermission/',
        'delete': '/role/delete'
    };

    $.when(
        getMenusList({url: '/category/list?type1=shop-category&size=' + maxSize, key: 'shop-category'}),
        getLoginUser())
        .done(function (list, user) {
            // list = list.list;
            var menusGroup = _.groupBy(list, 'level');
            var menusGroupByParentId = _.groupBy(list, 'pId');
            //var menuLeaf = list;
            if (bars && bars.length) {
                onClick(bars[0], function () {
                    dialogForm(
                        {
                            fullScreen: true,
                            title: '增加分类',
                            data: [
                                {
                                    type: 'text',
                                    label: '分类名称',
                                    require: true, // 是否必填
                                    placeholder: '请输入分类名称',
                                    name: 'name'
                                },
                                {
                                    type: 'text',
                                    label: '排序号',
                                    val: '0',
                                    placeholder: '请输入排序号',
                                    name: 'orderNum'
                                },
                            ],
                            submit: function (form) {
                                form.level = 0;
                                form.type1 = 'shop-category'; // 数据类型菜单
                                $.post('/category/add', form, function (res) {
                                    if (res.code == 200) {
                                        fnSuccess("操作成功");
                                        window.location.href = window.location.href;
                                    } else {
                                        fnFail("操作失败");
                                    }
                                })
                            }
                        });
                });
                onClick(bars[1], function () {
                    if (menusGroup[1] && menusGroup[1].length) {

                        dialogForm(
                            {
                                fullScreen: true,
                                data: [
                                    {
                                        type: 'select',
                                        label: '上级分类',
                                        require: true, // 是否必填
                                        placeholder: '请选择上级分类名称',
                                        name: 'pId',
                                        options: menusGroup[0]
                                    },
                                    {
                                        type: 'text',
                                        label: '分类名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入分类名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: '0',
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                shown: function () {
                                    initSelect({
                                        data: menusGroup[0],
                                        name: 'pId',
                                        label: '上级分类',
                                        change: function () {
                                            console.log(arguments);
                                        }
                                    })
                                },
                                submit: function (form) {
                                    form.level = 1;
                                    form.type1 = 'shop-category';
                                    $.post('/category/add', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功")
                                            window.location.href = window.location.href;
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    } else {
                        fnFail("请首先创建1级分类");
                    }
                });
                onClick(bars[2], function () {
                    if (menusGroup[1] && menusGroup[1].length) {

                        dialogForm(
                            {
                                fullScreen: true,
                                data: [
                                    {
                                        type: 'select',
                                        label: '上级分类',
                                        require: true, // 是否必填
                                        placeholder: '请选择上级分类名称',
                                        name: 'pId',
                                        options: menusGroup[1]
                                    },
                                    {
                                        type: 'text',
                                        label: '分类名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入分类名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: '0',
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                shown: function () {
                                    initSelect({
                                        data: menusGroup[1],
                                        name: 'pId',
                                        label: '上级分类',
                                        change: function () {
                                            console.log(arguments);
                                        }
                                    })
                                },
                                submit: function (form) {
                                    form.level = 2;
                                    form.type1 = 'shop-category';
                                    $.post('/category/add', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功")
                                            window.location.href = window.location.href;
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    } else {
                        fnFail("请首先创建一级菜单");
                    }
                });
            }
            var group = _.filter(user.permissions, 'type');
            group = _.groupBy(group, 'id')
            var oneLevel = menusGroup['0'], treeNode = [];
            _.sortBy(oneLevel, 'orderNum').map(function (item, index) {
                if (menusGroupByParentId[item.id] && menusGroupByParentId[item.id].length) { // 包含二级菜单
                    var subArr = menusGroupByParentId[item.id];
                    _.sortBy(subArr, 'orderNum').map(function (item1, index) {
                        var thirdArr = menusGroupByParentId[item1.id];
                        if (thirdArr && thirdArr.length) { //
                            _.sortBy(thirdArr, 'orderNum').map(function (item2, index) {
                                item2.pId = item1.id;
                                item2.isParent = false;
                                item2.open = false;
                                treeNode.push(item2);
                            })
                            item1.pId = item.id;
                            item1.isParent = true;
                            item1.open = true;
                            treeNode.push(item1);
                        } else {
                            item1.pId = item.id;
                            item1.isParent = false;
                            item1.open = false;
                            treeNode.push(item1);
                        }
                    });
                }
                item.open = true;
                item.isParent = true;
                item.pId = 0;
                item.id = item.id
                treeNode.push(item);
            });

            console.log(treeNode);
            console.log(list);
            //debugger;
            var existId = (_.pluck(treeNode, 'id'));
            var allId = (_.pluck(list, 'id'));
            console.log(existId);
            console.log(allId);
            var subArr = (_.difference(allId, existId));
            console.log([existId.length, allId.length, subArr.length]);
            if (subArr.length > 0) {
                var $rm = $('<a href="javascript:void(0);" class="btn btn-primary">清理无用分类(' + subArr.length + ')</a>');
                onClick($rm, function () {
                    subArr.map(function (id) {
                        $.get('/offer/rm-never-used-category-by-id?id=' + id);
                    });
                    fnSuccess('清理完毕');
                    $rm.remove();
                })
                $('.toolbar').append($rm)

            }
            zTree({
                eleId: 'ztree',
                data: treeNode,
                click: function (e, eleId, nodeItem, orderNum) {
                    // if (menusGroupByParentId[nodeItem.id] && menusGroupByParentId[nodeItem.id].length);// 包含二级菜单
                    // if (!nodeItem.isParent) {

                    dialogForm(
                        {
                            fullScreen: false,
                            title: '修改',
                            data: [

                                {
                                    type: 'text',
                                    label: '分类名称',
                                    val: nodeItem.name,
                                    require: true, // 是否必填
                                    placeholder: '请输入分类名称',
                                    name: 'name'
                                },
                                {
                                    type: 'text',
                                    label: '排序号',
                                    val: '0',
                                    val: nodeItem.orderNum,
                                    placeholder: '请输入排序号',
                                    name: 'orderNum'
                                },
                            ],

                            submit: function (form) {
                                form.id = nodeItem.id;
                                $.post('/category/update', form, function (res) {
                                    if (res.code == 200) {
                                        fnSuccess("操作成功")
                                        window.location.href = window.location.href;
                                    } else {
                                        fnFail("操作失败");
                                    }
                                })
                            }
                        });
                    // }
                    return;
                }
            }, user.permissions)
        });


    function view(items) {
        var $data = $(this).data('json');
        $.when(getDataByUrl(config.getPermission + $data.id, '$permission' + $data.id))
            .then(function (list) {
                var menusGroup = _.filter(list, function (item) {
                    return item.type == 1;
                });
                var accessGroup = _.filter(list, function (item) {
                    return item.type == 0;
                });
                $('.view-c').html(renderMenus(menusGroup) + renderAccess(accessGroup));
            })
            .fail(function (e) {
                console.log(e);
            })
    }
});

