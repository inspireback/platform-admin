$(function () {
    var query = {}, fields = 'id|name|remark',
        $search = $('.search');
    var config = {
        getPermission: '/role/getPermission/',
        update: '/role/update',
    }, $json = JSON.parse(parseSearch().json);
    $('.label').html($json.roleName);
    onClick($('.btn-save'), function () {
        var ids = [], permissionNames = [];

        $('.view-c input[type=checkbox]:checked').map(function (index, item) {
            ids.push($(item).attr('id'));
            permissionNames.push($.trim($(item).closest('label').text()));
        });
        if (ids.length == 0) {
            fnFail("请选择至少一个权限")
        }
        $.post(config.update, {
            id: $json.id,
            permissionNames: permissionNames.join(','),
            permissions: ids.join(',')
        }, function (res) {
            if (res.code == 200) {
                alert("保存成功!");
                window.location = './index.html'
            } else {
                alert(res.error);
            }
        })
    });
    $.when(getDataByUrl(config.getPermission + '3', '$permission3'), getDataByUrl(config.getPermission + $json.id, '$permission' + $json.id))
        .then(function (list, _current) {
            var menusGroup = _.filter(list, function (item) {
                return item.type == 1;
            });
            var accessGroup = _.filter(list, function (item) {
                return item.type == 0;
            });
            var $permisssions = $(renderMenus(menusGroup, true) + renderAccess(accessGroup, true));
            _current.map(function (item) {
                if ($('#' + item.id, $permisssions).length) {
                    $permisssions.find('#' + item.id).attr('checked', 'checked').prop('checked', 'checked');
                }
            });
            $('.view-c').html($permisssions);
        });

    $loading.fadeOut();


    // 表单提交
    var $submit = $('#submit');
    $('.new-add').click(function () {
        $('#myModalLabel').html('新建');
        $('.add-user-modal-lg input[type=checkbox]').attr('checked', false).prop('checked', false);
        $('.add-user-modal-lg input[name=id]').val('');
        $('.add-user-modal-lg input[name=name]').val('');
        $('.add-user-modal-lg input[name=remark]').val('');
    });
    $("#user-form").validate({
        submitHandler: function (form_) {
            var form = {};
            $submit.button('loading');
            fields.split('|').map(function (item) {

                form[item] = form_[item].value
            });
            var url = config.create;
            if (form.id) { // is update?
                url = config.update;
            } else {
                delete form.id;
            }
            form.permissions = [];
            $('.per-list input:checked').map(function (item) {
                form.permissions.push($(arguments[1]).val());
            });
            if (!form.permissions.length) {
                $submit.button('reset');
                return false;
            }
            form.permissions = form.permissions.join(',');
            $.post(url, form, function (xhr, data) {
                if (xhr.code == 200) {
                    search({}, true);
                    $('.add-user-modal-lg').modal('hide');
                    fnSuccess && fnSuccess('保存成功');
                } else {
                    alert(xhr.error);
                }
                $submit.button('reset');
            });
            return false;
        }
    });

});