$(function () {
    var $search = parseSearch(), arg = {}, tag = '';
    console.log($search);
    function operRes(res) {
        if (res.code == 200) {
            fnSuccess('操作成功');
            search({}, true);
        } else {
            fnFail(res.error || '操作失败');
        }
    }
    var $search = parseSearch();


   ;

    // if($search.sourceId){
    //     config.query = '/info/queryComWithDr?sourceId='+$search.sourceId+'&drType='+$search.drType+'&'
    // };
    var btns2 =[
        {
            label: '新建',
            onClick: function () {
                return window.location.href = './edit.html?type=test'+
                ($search.sourceId?('&sourceId='+$search.sourceId+'&drType='+$search.drType+'&'):'')
            }
        },
        {
            label: '修改',
            class_: 'update',
            onClick: function () {
                var $json = $(this).data('json');
                return window.location.href = './edit.html?type=test&id=' + $json.id+
                    ($search.sourceId?('&sourceId='+$search.sourceId+'&drType='+$search.drType+'&'):'');
            }
        },
        {
            label: '删除',
            class_: 'delete',
            onClick: function () {
                var $json = $(this).data('json');
                console.log($json);
                if (confirm('确认删除？')) {
                    $.post('/info/delete/', {id: $json.id}, function (res) {
                        if (res.code == 200) {
                            search({}, true);
                            fnSuccess('删除成功！');
                        }
                    })
                }
            }
        },
        {
            label: '发布<=>草稿',
            onClick: function () {
                var $json = $(this).data('json');
                var label = $json.status == '1' ? '草稿' : ($json.status == '0' ? '发布' : (''));
                if (confirm('确认' + label + '？')) {
                    $.post('/info/update', {
                        id: $json.id,
                        status: $json.status == '1' ? 0 : '1',
                    }, function (res) {
                        if (res.code == 200) {
                            fnSuccess(label + '操作成功');
                            search({}, true);
                        } else {
                            fnFail(label + '操作失败');
                        }
                    })
                }
            }
        },
    ];
    var diff = {


        "test": {//测试题
            query:$search.sourceId? ('/info/queryComWithDr?sourceId='+$search.sourceId+'&drType='+$search.drType+'&'):'/info/queryPlain?type1=9&cType=test',//,
            complexItems: [],
            btns: btns2,
            destroyBtn: function () {
                disableBtn(btns[1]);
                disableBtn(btns[2]);
                disableBtn(btns[3]);
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                if ($item.status != 1) { //发布状态不能修改、删除
                    enableBtn(btns[1], $item);
                    enableBtn(btns[2], $item);
                }
                enableBtn(btns[3], $item);
            },
            rendered: function () {
                onClick($('.detail'), function () {
                    var ind = layer.open({
                        type: 2,
                        title: $(this).html(),
                        shadeClose: true,
                        shade: false,
                        maxmin: true, //开启最大化最小化按钮
                        fullScreen: true,
                        content: $(this).data('href')
                    });
                    layer.full(ind);
                })
            },
            fields: [
                {name: 'id', label: 'ID'},
                {name: 'summary', label: '问题描述', enableSearch: true},
                {
                    name: 'category', label: '类型', filter: function (item) {
                    return item.category == '1' ? '单选' : '多选'
                }
                },
                {
                    name: 'content', label: '答案选项', filter: function (item) {
                    if (!item.content)return '';
                    return item.content.split('<>')[0].split(":")
                }
                },
                // {name: 'source', label: '答案'},
                // {name: 'read', label: '分值'},
                {
                    name: 'status', _sort: 'status', label: '状态',
                    filter: function (item) {
                        return (item.status == 1 ? '<span class="label label-success">发布</span>' : (item.status == 0 ? '<span class="label label-danger">草稿</span>' : (item.status == 0 ? '删除' : '')) );
                    }
                }

            ],
        },
    };
    if ($search.type) {
        arg = diff[$search.type];
        if (!arg) {
            return $('.table').html('<p style="text-align: center;line-height: 150px;font-size: 16px;">此功能开发中,敬请期待！</p>');
        }
    }
    var btns = arg.btns.map(function (item) { // 设置分类
        var btn = genBtn(item);
        if (item.onClick) onClick(btn, item.onClick)
        return btn;
    });
    setBtn(btns);


    function search(obj, isForce, isExport) {
        arg.destroyBtn();
        return gSearch({
            table: '.table',
            url: arg.query,
            query: obj,
            isForce: isForce,
            isExport: isExport,
            render: function (res) {
                var _$table = $('.table-responsive .table');
                destroyBtn();
                _$table.$table($.extend(TABLE_SETTING, {
                    checkbox: true, // 是否显示复选框
                    seqNum: true, // 是否显示序号
                    sort: search, //排序回调函数
                    destroyBtn: arg.destroyBtn, //禁用按钮
                    createBtn: arg.createBtn, // 启用她扭
                    toolbar: '.toolbar',
                    enableSetting: true,//允许自定义显示列
                    // rendered:function(){
                    //    
                    // },
                    search: search,
                    showSearchComplex: true,
                    rendered: arg.rendered || function () {
                    },
                    fields: arg.fields,
                    data: arg.listFilter ? arg.listFilter(res) : res || [],
                }, arg.dateSearch ? { // 允许日期区间搜索
                    complexItems: [
                        {label: "", name: 'date', dateRange: true},
                    ]
                } : {}));
            }
        }, obj.first ? false : true);
    }

    search({}, true);
})

