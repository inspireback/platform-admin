$(function () {
    // onClick($('#layerDemo'),function () {
    // layerAlert({
    //     type:2,
    //     title:'title',
    //     width:'800px',
    //     body:['/admin/sys/category/imgs.html'],
    //     full:true,
    // })
    // })

    upload('上传图片',{
        noCrop:true,
        class_:'#layerDemo',
        uploadBtn:$('#layerDemo'),
        url:"/api/upload",
        submit:function (obj) { }

    },function (res) {
        // console.log(res);
    })

    search({},false);
    function search(obj, isForce) {

        fnLoading()
        obj = obj||{};
        isForce = isForce||false;
        gSearch({
            url: obj.url||'/file/list',
            query: {},
            table: true,
            isForce: isForce,
            render: function (list, opt) {
                $("#test1").nextAll().replaceWith('');
                list.map(function (item) {
                    $('.pictures').append('<li' +
                        ' style="box-sizing:content-box;background: #fff;padding:10px;margin:0px;width:180px;height:180px;overflow: hidden;border-radius: 10px;"' +
                        '><div class="span" style="border-radius: 10px; background: #ECECEC;"><img class="appendImg"' +
                        ' style="display: block;max-width:100%;margin: 0 auto;width: auto;height:auto;"'+
                        ' src="'+item.url+'" id="'+item.id+'"/></div><img class="imgss" style="display:none" src="../../imgs/backstage/21.png"/></li>')
                })

                var itemWidth= parseInt($('.pictures').width()/10);
                // hideLoading();
                $('.pictures').css({
                    padding:0,
                    margin:0
                })
                setTimeout(function(){
                    $('.pictures li').css({
                        border:'none',
                        background:'#fff',
                        'box-sizing':'border-box',
                        margin:0,
                        padding:10
                    })
                    $('.pictures').css({
                        "padding":padding,
                        'box-sizing': 'border-box'
                    })
                    $('#test1').css({
                        width:'160px',
                        height:'180px',
                        margin:"0 10px"
                    })

                },200);
                var leftPadding = ($('.pictures').width()-10*itemWidth)/2;
                var padding = '0 '+leftPadding+'px!important';
            }
        }, false);
    }
    onClick($("#searchPic"),function(){
        var name = $(".photo-name").val();
        search({
            url: "/file/list?name="+name,
        },false);


        $(".photo-name").val('')
        $("select").val('请选择照片类型')
    })
    onClick($(".reset"),function(){
        $(".photo-name").val('')
        $("select").val('请选择照片类型')
    })
    function initImg(url, isThumbnail, fileId) {
        var item = ejs.render($('#img-item-template').html(), {
            url: url,
            isThumbnail: isThumbnail,
            id: fileId,
        });

        $(item).insertBefore($('.item-trigger').closest('.col-sm-4'));
    }


    /**
     * 从图片库中选择
     */

    onClick($(".select-from-libs"), function () {
        imageSelector({
            multi: true,
            title: '',
            body: $('#imgs').html()
        }, {
            ok: function (arr, cb, t) {
                arr = arr || [];
                arr.map(function (item) {
                    initImg(item.url, false, item.id)
                    cb(t)
                })
            },
            cancel: function (cb, t) {
                cb(t)
            },
            dbclick: function ($ele) {
                $ele.find('img.appendImg').attr('src')
                $ele.find('img.appendImg').attr('id')

            },
            click: function ($ele) {
                $ele.find('img.appendImg').attr('src')
                $ele.find('img.appendImg').attr('id')
            }
        })
    })

})