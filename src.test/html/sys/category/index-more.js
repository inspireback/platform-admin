function moreConstFn($search, search) {
    return {
        'type': { //
            query: '/const/' + ($search.type1 == 10 ? 'category' : 'dataR'),//分页查询URL,
            rendered: function () {
            },
            btns: [],
            beforeRender(data){
                return Object.keys(data).map(function (key) {
                    var item = data[key].split(':');
                    return {
                        key: key,
                        value: item[0],
                        label: item[1]
                    }
                })
            },
            destroyBtn: function () {
            },
            createBtn: function (_this) {
            },
            fields: [
                {name: 'key', label: 'KEY'},
                {name: 'value', label: '类型值'},
                {
                    name: 'label', label: '描述',
                    filter: function (item) {
                        return viewMore(item.label || ''); // 预览更多内容
                    }
                },
                {
                    label: '管理', filter: function (item) {
                    return '<a href="./index-v2.html?value=' + item.value + '&label='+item.label+'" >查看</a>'
                }
                },

            ],
        },
    };
};