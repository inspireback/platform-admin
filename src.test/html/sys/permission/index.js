$(function () {


    var btns = [
        {
            label: '增加' + tag,
            class_: 'create',
            onClick: function () {
                dialogForm(
                    {
                        title: "增加分类",
                        data: [
                            {
                                type: 'text',
                                label: '分类名称',
                                require: true, // 是否必填
                                placeholder: '请输入分类名称',
                                name: 'name'
                            },
                            {
                                type: 'text',
                                label: '英文名称',
                                val: $json.en,
                                require: true, // 是否必填
                                placeholder: '请输入英文名称',
                                name: 'en'
                            },
                            {
                                type: 'text',
                                label: '排序号',
                                val: '0',
                                placeholder: '请输入排序号',
                                name: 'orderNum'
                            },
                        ],
                        submit: function (form) {
                            form.level = 1;
                            form.type1 = 'info'; // 数据类型菜单
                            $.post('/category/add', form, function (res) {
                                if (res.code == 200) {
                                    fnSuccess("操作成功");
                                    search({}, true);
                                    closeLayer($index);
                                } else {
                                    fnFail("操作失败");
                                }
                            })
                        }
                    });
            }
        },
        {
            label: '修改' + tag,
            class_: 'update',
            onClick: function () {
                var $json = $(this).data('json');
                console.log($json);
                var $index = dialogForm(
                    {
                        title: "修改分类",
                        data: [
                            {
                                type: 'text',
                                label: '分类名称',
                                val: $json.name,
                                require: true, // 是否必填
                                placeholder: '请输入分类名称',
                                name: 'name'
                            },
                            {
                                type: 'text',
                                label: '英文名称',
                                val: $json.en,
                                require: true, // 是否必填
                                placeholder: '请输入英文名称',
                                name: 'en'
                            },
                            {
                                type: 'text',
                                label: '排序号',
                                val: $json.orderNum,
                                placeholder: '请输入排序号',
                                name: 'orderNum'
                            },
                        ],
                        submit: function (form) {
                            form.id = $json.id; // 数据类型菜单
                            form.type1 = 'info'; // 数据类型菜单
                            $.post('/category/update', form, function (res) {
                                if (res.code == 200) {
                                    fnSuccess("操作成功");
                                    search({}, true);
                                    closeLayer($index);
                                } else {
                                    fnFail("操作失败");
                                }
                            })
                        }
                    });
            }
        },
        {
            label: '上传' + tag,
            class_: 'upload',
            onClick: function () {
                var $json = $(this).data('json');
                console.log($json);
                window.location.href= './img.html?id='+$json.id+'&noCrop=1'

            }
        }
    ].map(function (item) { // 设置分类
        var btn = genBtn(item);
        if (item.onClick) onClick(btn, item.onClick)
        return btn;
    });
    setBtn(btns);
    destroyBtn();
    function destroyBtn() {
        disableBtn(btns[2]);
        disableBtn(btns[1]);
    }

    var query = {}, coms = [],
        $search = $('.search'), $searchBar = $('.search-btn'),
        $update = $('.btn-update'), $delete = $('.btn-delete'),
        $create = $('.btn-create'),
        $setting = $('.btn-setting'),
        $log = $('.btn-log'),
        strHtml = $('#form-template').html();
    var config = {
        query: '/role/getPermission/3?',//分页查询URL,
    };
    function search(obj, isForce) {
        var query_ = {};
        $.extend(query_, query, obj instanceof Object ? obj : {});
        if (!isForce) {
            if (isSameQueryStr(query_, query)) { //查询条件和上次一样，取消请求
                return $loading.fadeOut();
            }
        }
        query = query_;
        query.timestamp = Date.now();
        $.get(config.query + ajaxArgStrGenerator(query), function (res) {
            var _$table = $('.table-responsive .table');
            destroyBtn();
            _$table.$table($.extend(TABLE_SETTING, {
                checkbox: false, // 是否显示复选框
                seqNum: true, // 是否显示序号
                sort: search, //排序回调函数
                destroyBtn: destroyBtn, //禁用按钮
                createBtn: createBtn, // 启用她扭
                toolbar: '.toolbar',
                enableSetting: true,//允许自定义显示列
                search: function (obj) { //搜索框查询函数
                    search(obj)
                },
                fields: [
                    {name: 'permissionName',label: '权限名称'},
                    {name: 'description',label: '描述'},
                    {name: 'url',label: 'URL'},
                    {name: 'remark',label: '排序号'},
                ],
                data: res.list || [],
            }));
            var page = res.page;
            pageInit(page, query, search);
        });
    }
    function destroyBtn() {

    }
    function createBtn(_this) {
        destroyBtn();
        var $item = ($(_this).data('json'));
    }

    search({}, true);
});