$(function () {
    var query = {}, $nav = $('#navs').html(), search_ = parseSearch();

    var config = {
        com: '/sysLog/query?objectId=' + search_.id + '&type=1',
        dep: '/sysLog/query?objectId=' + search_.id + '&type=2',
        otherReceivables: '/sysLog/query?objectId=' + search_.id + '&type=14',
        otherPay: '/sysLog/query?objectId=' + search_.id + '&type=15',
        applyPay: '/sysLog/query?objectId=' + search_.id + '&type=20',
        purchaseOrderBill: '/purchaseOrderBill/log/query?orderBillId=' + search_.id + '&type=1',
        user: '/sysLog/query?type=3',
        login: '/sysLog/query?type=4',
        goods: '/sysLog/query?objectId=' + search_.id + '&type=7',
        offer: '/sysLog/query?objectId=' + search_.id + '&type=8',
        account: '/sysLog/query?objectId=' + search_.id + '&type=11',
        order: '/sysLog/query?objectId=' + search_.id + '&type=9',
        orderPay: '/sysLog/query?objectId=' + search_.id + '&type=9',
        ladingBill: '/sysLog/query?objectId=' + search_.id + '&type=19',
        applyBack: '/sysLog/query?objectId=' + search_.id + '&type=9',
        advanceReceipt: '/sysLog/query?objectId=' + search_.id + '&type=12',
        advancePay: '/sysLog/query?objectId=' + search_.id + '&type=13',
        role: '/sysLog/query?objectId=' + search_.id + '&type=10',
        // otherReceivables: '/sysLog/query?objectId=' + search_.id + '&type=14',
    };
    var navs = {
        order: [
            {label: '订单管理', url: './../product/order/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        orderPay: [
            {label: '销售应收明细', url: './../gathering-finance/sale/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        ladingBill: [
            {label: '销售发货', url: './../store/express/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        offer: [
            {label: '商品管理', url: './../product/offer/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        applyBack: [
            {label: '应付款管理', url: 'javascript:void(0);'},
            {label: '销售应付明细', url: './../due-finance/sale/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        role: [
            {label: '权限管理', url: './../sys/role/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        com: [
            {label: '单位管理', url: './../sys/com/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        user: [
            {label: '系统管理', url: 'javascript:void(0);'},
            {label: '用户管理', url: './../sys/user/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        otherReceivables: [
            {label: '其他应收款管理', url: './../gathering-finance/other/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
         ],
        otherPay: [
            {label: '其他应付款管理', url: './../due-finance/other/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        purchaseOrderBill: [
            {label: '采购收货单管理', url: './../purchase/takeover/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        applyPay: [
            {label: '采购付款申请单', url: './../due-finance/purchase/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        login: [
            {label: '系统管理', url: 'javascript:void(0);'},
            {label: '登录日志', url: 'javascript:void(0)'},
        ],
        goods: [
            {label: '产品管理', url: './../product/goods/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],

        account: [
            {label: '应收款管理', url: 'javascript:void(0)'},
            {label: '账户管理', url: './../gathering-finance/account/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        advanceReceipt: [
            {label: '应收款管理', url: 'javascript:void(0)'},
            {label: '预收款单', url: './../gathering-finance/advance-receipt/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
        advancePay: [
            {label: '应付款管理', url: 'javascript:void(0)'},
            {label: '预付款单', url: './../due-finance/advance-pay/index.html'},
            {label: '操作日志', url: 'javascript:void(0)'},
        ],
    };

    $(ejs.render($nav, {
        navs: navs[search_.type]
    })).insertBefore('.content');
    function searchLog(obj) {
        var query_ = {};
        $.extend(query_, query, obj instanceof Object ? obj : {});
        var arg = [];
        if (isSameQueryStr(query_, query)) { //查询条件和上次一样，取消请求
            return;
        }
        query = query_;query.timestamp = Date.now(); 
        $.get(config[search_.type] +'&'+ ajaxArgStrGenerator(query), function (res) {
            var _$table = $('.table-responsive .table');
            destroyBtn();
            _$table.$table($.extend(TABLE_SETTING, {
                checkbox: false, // 是否显示复选框
                seqNum: true, // 是否显示序号
                sort: searchLog, //排序回调函数
                toolbar: '.toolbar',
                enableSetting: true,//允许自定义显示列
                search: function (obj) { //搜索框查询函数
                    searchLog(obj)
                },
                fields: [
                    {name: 'ip', _sort: 'ip', label: '访问IP', style: 'width:80px;',},
                    {
                        name: 'content',
                        _sort: 'content',
                        label: '日志内容',
                        style: 'white-space: inherit;',
                        enableSearch: true
                    },
                    {
                        name: 'name',
                        style: 'width:80px;',
                        _sort: 'name', label: '操作用户'
                    },
                    {
                        style: 'width:120px;',
                        name: 'createAt',
                        _sort: 'create_at',
                        label: '操作时间',
                        enableSort: true,
                        filter: function (item) {
                            return formatTimeStrWithoutYear(item.createAt);
                        }
                    },
                ],
                data: res.list || [],
            }));
            var page = res.page;
            pageInit(page, query, searchLog);
        });
    }

    searchLog();
});