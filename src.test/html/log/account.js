$(function () {
    var query = {},
        $update = $('.btn-update'),
        $create = $('.btn-create'),
        strHtml = $('#form-template').html(), search_ = parseSearch();
    var config = {
        query: '/payType/query?type=' + search_.type+'&',//分页查询URL,
        create: '/payType/create',//create,
        update: '/payType/update',
    };

    function search(obj,isForce) {
        var query_ = {};
        $.extend(query_, query, obj instanceof Object ? obj : {});
        $loading.fadeIn();
        if(!isForce){
            if (isSameQueryStr(query_,query)) { //查询条件和上次一样，取消请求
                return;
            }
        }
        query = query_;query.timestamp = Date.now(); 
        $.get(config.query + ajaxArgStrGenerator(query), function (res) {
            var _$table = $('.table-responsive .table');
            destroyBtn();
            _$table.$table($.extend(TABLE_SETTING, {
                checkbox: true, // 是否显示复选框
                seqNum: true, // 是否显示序号
                sort: search, //排序回调函数
                destroyBtn: destroyBtn, //禁用按钮
                createBtn: createBtn, // 启用她扭
                toolbar: '.toolbar',
                enableSetting: true,//允许自定义显示列
                search: function (obj) { //搜索框查询函数
                    search(obj)
                },
                fields: [
                    {name: 'name', _sort: 'name', label: '供货地'},
                    {name: 'order', _sort: 'order', label: '排序号'},
                    {name: 'remark', _sort: 'remark', label: '备注说明'},
                ],
                data: res.list || [],
            }));
            var page = res.page;
            pageInit(page, query, search);
        });
    }

    $update.unbind('click').on('click', function () {
        var $current = $(this).data('json'), self = this,
            $modal = _alert({
                title: '修改供货地"' + $current.name + '"',
                body: ejs.render(strHtml, $current),
                shown: function () {
                    validate($modal);
                },
            });
    });
    // onClick($log, function () {
    //     var $current = $(this).data('json');
    //     window.location = './../../log/log.html?id=' + $current.id + '&type=account';
    // });

    $create.unbind('click').on('click', function () {
        var $modal = _alert({
            title: '新建供货地',
            body: ejs.render(strHtml, {name: ''}),
            shown: function () {
                validate($modal);
            }
        })
    });
    function destroyBtn() {
        $update.data('json', null);
        disableBtn($update);
        // disableBtn($log);
    }

    function createBtn(_this) {
        destroyBtn();
        $update.data('json', ($(_this).data('json')));
        // $log.data('json', ($(_this).data('json')));
        enableBtn($update);
        // enableBtn($log);
    }

    search({},true);
    function validate(modal) {
        $('#registrationForm')
            .formValidation('destroy').formValidation($.extend({}, VALIDATION_SETTING, {
                fields: {
                    name: {
                        row: '.col-xs-7',
                        validators: {
                            notEmpty: {
                                message: '供货地必填'
                            },
                            stringLength: {
                                min: 2,
                                max: 40,
                                message: '供货地长度最少3位，最多40位'
                            },
                        }
                    },

                    order: {
                        row: '.col-xs-7',
                        validators: {
                            notEmpty: {
                                message: '排序号必填'
                            },
                        }
                    },
                }
            }))
            .off('success.form.fv').on('success.form.fv', function (e) {
                e.preventDefault();
                var form = parseSearch($(e.target).serialize());
                var url = '';
                if (form.id) { // is update?
                    url = config.update;
                } else {
                    url = config.create;
                    delete form.id;
                }
                if (form.name) {
                    form.name = form.name.trim();
                }
                $.post(url, form, function (xhr, data) {
                    if (xhr.code == 200) {
                        fnSuccess(form.id ? '保存成功' : '创建成功!');
                        search({},true);
                        modal && modal.modal('hide');
                    } else {
                        alert(xhr.error || xhr.msg);
                    }
                });
            });
    }
});