$(function () {
    var config = {
        query: '/salePerson1/query?type1=0&',//分页查询URL,
    };

    function search(obj, isForce, isExport) {
        return gSearch({
            table: '.table',
            url: config.query,
            query: obj,
            isForce: isForce,
            isExport: isExport,
            render: function (res) {
                var _$table = $('.table-responsive .table');
                destroyBtn();
                _$table.$table($.extend(TABLE_SETTING, {
                    checkbox: true, // 是否显示复选框
                    seqNum: true, // 是否显示序号
                    sort: search, //排序回调函数
                    destroyBtn: destroyBtn, //禁用按钮
                    createBtn: createBtn, // 启用她扭
                    toolbar: '.toolbar',
                    enableSetting: true,//允许自定义显示列
                    search: search,
                    showSearchComplex: true,
                    complexItems: [
                        {label: "", name: 'date', dateRange: true},
                    ],
                    rendered: function () {
                        selectGoods({
                            name: 'goodsId', //
                            change: function (val, $ele) {
                                search({goodsId: val})
                            }
                        })
                    },
                    'export': false,
                    fields: [
                        {
                            name: 'userName', label: '头像',
                            filter: function (item) {
                                if (item.avatar) {
                                    return '<img src="'+item.avatar+'" style=" \
                                    width: 50px; \
                                    border-radius: 5px; \
                                    " />';
                                } else {
                                    return '';
                                }
                            }
                        },
                        {name: 'userName', label: '昵称'},
                        {name: 'name', label: '姓名'},
                        {name: 'phone', label: '手机号'},
                        {
                            name: 'CustomerCount', label: '上季度成长值',
                            filter: function (item) {
                                return item.customerCount || 0;
                            }
                        },
                        {
                            name: 'saleCount', label: '本季度成长值',
                            filter: function (item) {
                                return item.saleCount || 0;
                            }
                        },
                        {
                            name: 'level', label: '会员级别',
                            filter: function (item) {
                                if (item.level == '0') {
                                    return '白卡';
                                }
                                if (item.level == '1') {
                                    return '绿卡';
                                }
                                if (item.level == '2') {
                                    return '银卡';
                                }
                                if (item.level == '3') {
                                    return '金卡';
                                }
                                if (item.level == '4') {
                                    return '黑卡/荣誉会员';
                                }
                                return '白卡';
                            }
                        },
                    ],
                    data: res || [],
                }));
            }
        }, obj.first ? false : true);
    }

    search({}, true);
});