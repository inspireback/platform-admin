$(function () {
    var config = {
        query: '/saleperson/queryUserWx?',//分页查询URL,
    };
    var $update = $('.btn-update');
    function destroyBtn(){
        disableBtn($update);
    }
    function createBtn(_this){
        destroyBtn();
        var item = $(_this).data('json');
        enableBtn($update,item)

    }
    onClick($update,function(){
        var $json = $(this).data('json');
        console.log($json);
        var $index = dialogForm(
            {
                title: "修改佣金",
                data: [
                    {
                        type: 'text',
                        label: '用户昵称',
                        val: $json.nickName || '',
                        placeholder: '请输入描述',
                        name: 'nickName'
                    },
                    {
                        type: 'text',
                        label: '现有佣金',
                        val: $json.pointsNow  || '',
                        placeholder: '请输入现有佣金',
                        name: 'pointsNow'
                    },
                    {
                        type: 'text',
                        label: '累计佣金',
                        val: $json.points  || '',
                        placeholder: '请输入累计佣金',
                        name: 'points'
                    },

                ],
                submit: function (form) {
                    form.id = $json.id; // 数据类型菜单
                    $.post('/userWx/update', form, function (res) {
                        if (res.code == 200) {
                            fnSuccess("操作成功");
                            search({}, true);
                            closeLayer($index);
                        } else {
                            fnFail("操作失败");
                        }
                    })
                }
            });
    })
    function search(obj, isForce, isExport) {
        return gSearch({
            table: '.table',
            url: config.query,
            query: obj,
            isForce: isForce,
            isExport: isExport,
            render: function (res) {
                var _$table = $('.table-responsive .table');
                destroyBtn();
                _$table.$table($.extend(TABLE_SETTING, {
                    checkbox: true, // 是否显示复选框
                    seqNum: true, // 是否显示序号
                    sort: search, //排序回调函数
                    destroyBtn: destroyBtn, //禁用按钮
                    createBtn: createBtn, // 启用她扭
                    toolbar: '.toolbar',
                    enableSetting: true,//允许自定义显示列
                    search: search,
                    showSearchComplex: true,
                    complexItems: [

                    ],
                    rendered: function () {

                    },
                    'export': true,
                    fields: [
                        {
                            name: 'userName', label: '头像',
                            filter: function (item) {
                                if (item.headimgurl) {
                                    return '<img src="' + item.headimgurl + '" style=" \
                                    width: 50px; \
                                    border-radius: 5px; \
                                    " />';
                                } else {
                                    return '';
                                }
                            }
                        },
                        {name: 'nickName', label: '昵称', enableSearch: true,},
                        {name: 'phone', label: '手机号'},

                        {name: 'country', label: '国家',},
                        {name: 'province', label: '省',},
                        {name: 'city', label: '城市',},
                        {
                            name: 'level', label: '会员等级',
                            filter: function (item) {
                                return '等级' + item.level;
                            }
                        },
                        {name: 'points', label: '累计佣金',},
                        {name: 'pointsNow', label: '现有佣金',},
                        {
                            name: 'createAt', label: '注册时间',
                            filter: function (item) {
                                return formatTime(item.createAt || new Date());
                            }
                        },
                    ],
                    data: res || [],
                }));
            }
        }, obj.first ? false : true);
    }

    search({}, true);
});