/***
 * todo:
 * 1. 自定义角色时，如何判定某一新定义角色为运营角色？
 * 2. 插入数据是接口层面增加创建人
 *
 */
$(function () {
    var query = {}, fields = 'id|name|code|unit|qualitySpec|operator|isDanger|isDrugs',
        $search = $('.search'), $searchBar = $('.search-btn');
    var operate = {}, operator_list = [];
    var config = {
        coloum_num: 8, //列数目
        query: '/saleperson/querySaleManager?',//分页查询URL,
        update: '/goods/update',
        'delete': '/goods/delete',
        user: '/user/operator' //运营角色用户
    };


    $searchBar.click(function () {
        $(this).button('loading');
        console.log({keyword: $search.val()});
        search({keyword: $search.val()});
    });
    function search(obj,isForce) {
        var query_ = {};
        $.extend(query_, query, obj instanceof Object ? obj : {});
        $loading.fadeIn();
        var arg = [];
        if (!isForce) {
            if (isSameQueryStr(query_, query)) { //查询条件和上次一样，取消请求
                return $loading.fadeOut();
            }
        }
        Object.keys(query_).map(function (key) {
            arg.push(key + '=' + query_[key])
        });
        query　= query_;
        $.get(config.query + arg.join('&'), function (res) {
            $searchBar.button('reset');
            if (res.list.length) {
                render(res.list)
            } else {
                render(noData(config.coloum_num));
                $('.reset-query').click(function () {
                    $search.val('');
                    $searchBar.click();
                })
            }
            var page = res.page;
            pageInit(page,query,search)


        });
    }

    function render(list) {
        // $('.table-responsive .table tbody')

        $('.table-responsive .table tbody').html('');
        if (list instanceof Array) {
            list.forEach(function (item, index) {
                // re
                $('.table-responsive .table tbody')
                    .append('<tr> \
                    <td scope="row">' + (index + 1) + ' \
                </td> \
                <td>' + (item.name || '') + '</td> \
                <td>' + item.userName + '</td> \
                <td>' + item.phone + '</td> \
                <td><a href="./detail.html?id=' + item.id + '&name=' + item.userName + '">' + item.salesCount + '</a></td> \
                </tr>')
            });
        } else {
            $('.table-responsive .table tbody').html(list);
        }
        $loading.fadeOut();
    }

    search({},true);

    // 表单提交
    var $submit = $('#submit');
    $submit.on('click', function () {
        var $btn = $(this).button('loading');
    });

    $("#user-form").validate({
        submitHandler: function (form_) {
            var form = {};
            fields.split('|').map(function (item) {
                if (form_[item].type == 'checkbox') {
                    form[item] = $(form_[item]).is(':checked') ? '1' : '0';
                } else {
                    form[item] = form_[item].value
                }
                form['operatorId'] = Object.keys(operate).toString();
                form['operator'] = Object.keys(operate).map(function (item, index) {
                    return operate[item]
                }).toString();

            });
            var url = config.create;
            if (form.id) { // is update?
                url = config.update;
            } else {
                delete form.id;
            }
            $.post(url, form, function (xhr, data) {
                $submit.button('reset');
            });
            return false;
        }
    });
    $('.table th .fa').click(function (e) {
        var sort = {sortField: '', sortOrder: ''};
        sort.sortField = $(this).data('sort');
        if ($(this).hasClass('fa-sort') || $(this).hasClass('fa-sort-desc')) {
            sort.sortOrder = 'ASC';
            $(this)
                .removeClass('fa-sort-desc fa-sort')
                .addClass('fa-sort-asc');
            $(this)
                .closest('th')
                .siblings('th')
                .find('.fa')
                .removeClass('fa-sort-desc fa-sort-asc')
                .addClass('fa-sort');
        } else if ($(this).hasClass('fa-sort-asc')) { //asc
            sort.sortOrder = 'DESC';
            $(this)
                .removeClass('fa-sort-asc fa-sort')
                .addClass('fa-sort-desc');
            $(this)
                .closest('th')
                .siblings('th')
                .find('.fa')
                .removeClass('fa-sort-desc fa-sort-asc')
                .addClass('fa-sort')
        }
        search(sort);
    })
});