$(function () {
    var query = {}, fields = 'id|name|shortName|admin|address|showDriver|phone|fax|order|remark',
        $search = $('.search'), $searchBar = $('.search-btn');
    var config = {
        query: '/ladingBill/get?',//分页查询URL,
        update_labing_bill_actual: '/ladingBill/update_labing_bill_actual',

    };

    $.get(config.query + 'id=' + parseSearch().id, function (res) {
        if (res.data) {
            render(res.data)
        }
    });

    function render(data) {
        // $('.table-responsive .table tbody')
        var list = data.ladingBillDetail;

        data = data.ladingBill;
        $('.table-responsive .table.summary tbody').html('');
        $('.table-responsive .table.summary tbody')
            .append("<tr><td>提单号</td><td>" + data.no + "</td></tr>")
            .append("<tr><td>提单类型</td><td>" + (data.type == 0 ? '车号' : (data.type == 1 ? '介绍信' : (data.type == 2 ? '转货权' : ''))) + "</td></tr>")
            .append("<tr><td>转货公司（车号）</td><td>" + data.carId + "</td></tr>")
            .append("<tr><td>数量</td><td>" + data.numberThisTime + "</td></tr>");
        $('.table-responsive .table.detail tbody').html('');
        list.map(function (item, index) {
            $('.table-responsive .table.detail tbody').append('<tr>' +
                '<td>' + (1 + index) + '</td>' +
                '<td>' + item.paperAccount + '</td>' +
                '<td>' + item.price + '</td>' +
                '<td>' + item.billBatch+ '</td>' +
                '<td>' + item.number + '</td>' +
                '<td>' + item.actual + '</td></tr>')
        });

        $('.opt-actual').click(function () {
            var val = prompt("请输入实提数量[最大" + $(this).data('max') + "]", $(this).data('max'));
            if (0 < val && val <= $(this).data('max')) {
                var form = {
                    id: $(this).data('id'),
                    actual: val
                };
                $.post(config.update_labing_bill_actual, form, function (xhr, data) {
                    if (xhr.code == 200) {
                        $.get(config.query + 'id=' + parseSearch().id, function (res) {
                            if (res.data) {
                                render(res.data)
                            }
                        });
                    } else {
                        alert(xhr.error)
                    }
                });
            } else {
                alert('数量不合法')
            }
        });

        $loading.fadeOut();
    }
});