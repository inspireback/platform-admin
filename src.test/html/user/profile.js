$(function () {
    var query = {};

    // 表单提交
    var $submit = $('#submit');

    $('#user-form').validate({
        submitHandler: function (form_) {
            var fields = 'password';
            $submit.button('loading');

            var form = {};
            fields.split('|').map(function (item) {
                form[item] = form_[item].value
            });
            if(form.password){
                form.password = md5(form.password);
            }
            $.post('/user/reset', form, function (xhr) {
                if (xhr.code == 200) {
                    alert('修改成功!');
                    window.location = '../main.html';
                } else {
                    fnFail(xhr.error)
                }
            });
            $submit.button('reset');
            return false;
        }
    });


});