$(function () {
    var config = {
        coloum_num: 3, //列数目
        query: '/info/query?',//分页查询URL,
        create: '/info/create',//create,
        update: '/info/update',
        get: '/info/get',
        'delete': '/info/delete',
    };
    var query = {}, fields = 'title',
        $search = $('.search'), $searchBar = $('.search-btn');
    var self = this;
    var search = parseSearch();
    if(search.noHead=='1'){
        $('.navbar').remove();
        $('.cum-header').remove();
        $('.sub-body').css({'padding-top':0});
    }
    var self = this;
    if (search && (search.id + '').length) {
        if (search.id == 0 || search.id) {
            $.get(config.get + '?id=' + search.id, function (data) {
                if (data.code == 200) {
                    data = data.data;

                    $('.title').html(data.title);
                    $('.info-content').html(data.content.indexOf('%3D')>-1?unescape(data.content):data.content);
                    $('.summary').html('<strong>摘要：</strong>'+(data.summary||'暂无'));
                    $('.des').html("作者: " + data.author + '  |  发布时间：' + moment(data.publish).format('YYYY-MM-DD HH:mm:ss'));
                }
            })
        }
    }

});