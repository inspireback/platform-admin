$(function () {
    var searchObj = parseSearch();
    if (searchObj.id == 0 || searchObj.id) {
        $.get('/order/get?id=' + searchObj.id, function (data) {
            if (data.code == 200) {
                var clientele = data.data;
                var fields = 'no|goodsName|actualPrice|number|total|deliverNumber|payStatus|deliverStatus|checkStatus|status|' +
                    'clienteleName|clienteleUserName|clienteleUserPhone|pickUpMethod|checkTime|checker' +
                    '|freeStoreType|deliveryType|payMethod|payDate|payCondition|createAt';
                fields.split('|').map(function (key) {

                    if (key == 'createAt') {
                        $('.' + key).html(moment(clientele[key]).format('YYYY-MM-DD HH:mm:SS'));
                    } else if (key == 'payStatus') {
                        $('.' + key).html((clientele[key] == 0 ? '未付款' : (clientele[key] == 1 ? '已付款' : '')));
                    } else if (key == 'deliverStatus') {
                        $('.' + key).html((clientele[key] == 0 ? '未发货' : (clientele[key] == 1 ? '已全部发货' : (clientele[key] == 2 ? '部分发货' : ''))));
                    } else if (key == 'status') {
                        $('.' + key).html((clientele[key] == 0 ? '正常' : (clientele[key] == 1 ? '终止执行' : (clientele[key] == 2 ? '强制执行完毕' : ''))));
                    } else if (key == 'checkStatus') {
                        $('.' + key).html((clientele[key] == 0 ? '未审核' : (clientele[key] == 1 ? '通过' : (clientele[key] == 2 ? '拒绝' : ''))));
                    } else if (key == 'pickUpMethod') {
                        $('.' + key).html((clientele[key] == 0 ? '自提' : (clientele[key] == 1 ? '送货至需方仓库' : '')));
                    } else if (key == 'freeStoreType') {
                        $('.' + key).html((clientele[key] == 0 ? '合同签订日起' : (clientele[key] == 1 ? '交割日起' : '')) + clientele['freeStoreDay'] + '天');
                    } else if (key == 'deliveryType') {
                        $('.' + key).html(moment(clientele['deliveryDay']).format('YYYY-MM-DD HH:mm:ss') + (clientele[key] == 0 ? '' : (clientele[key] == 1 ? '至' + moment(clientele['deliveryDayEnd']).format('YYYY-MM-DD HH:mm:ss') : '')));
                    } else if (key == 'payMethod') {
                        $('.' + key).html((clientele[key] == 0 ? '银行电汇' : (clientele[key] == 1 ? '承兑汇票' : '国内信用证')));
                    } else if (key == 'createAt' || key == 'payDate' || (key == 'checkTime' && clientele[key])) {
                        console.log(key)
                        $('.' + key).html(moment(clientele[key]).format('YYYY-MM-DD HH:mm:ss'));
                    } else {
                        $('.' + key).html(clientele[key]);
                    }
                })
            } else {
                alert(data.error);
            }
        })
    }
})