/**
 * Created by suweiming on 2018/11/19.
 */
$(function () {
    onClick($('.input-no'), function () {

    });
    var $search = parseSearch();

    $.get('/order/group/' + $search.id, function (res) {
        res.data.group.status = res.data.group.status == '1' ? '<span class="label label-success">进行中</span>' : (res.data.group.status == '2' ? '<span class="label label-danger">已完成</span>' : '<span class="label label-cancel">已取消</span>');
        $('.content').html(ejs.render($('#group-detail').html(), res.data));
        onClick($('.order-detail'), function () {
            layerAlert({
                type: 2,
                full: true,
                body: [$(this).data('href')],
                title: '订单详情,订单编号:' + $(this).html()
            })
        })
    })

})