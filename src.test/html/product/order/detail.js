/**
 * Created by suweiming on 2018/11/19.
 */
$(function () {
    // 填入运单号
    onClick($('.input-no'), function () {

    });
    // 设置发货
    // onClick($('.input-no'), function () {

    // });
    var $search = parseSearch();

    $.get('/order/order-detail-no-spec?id=' + $search.id, function (res) {
    // $.get('/order/'+($search.api||'exchange')+'Detail?id=' + $search.id, function (res) {
    //     console.log(res);
        res.data.order.payStatusLabel = res.data.order.payStatus == '1' ? '已付款' : '未付款';
        res.data.order.status = res.data.order.status ;
        res.data.order.statusLabel = res.data.order.status == '-1' ? '已取消[付款回退]' : (res.data.order.status == '0' ? '进行中' : (res.data.order.status == '-2' ? '超时未付款' : "已完成"));
        res.data.order.deliverStatusNum = res.data.order.deliverStatus;
        // res.data.order.deliverStatus = res.data.order.deliverStatus == '0' ? '<span class="label label-danger">未发货</span>' : (res.data.order.deliverStatus == '1' ? '<span class="label label-warning">已发货</span>' : (res.data.order.deliverStatus == '2' ? "<span class='label label-success'>已签收</span>" : ''));
        $('.content').html(ejs.render($('#order-detail').html(), res.data));
        onClick($('.btn-cancel'), function () {
            if (confirm('确认取消订单？')) {
                $.post('/order/cancel/' + $search.id, {}, function (res) {
                    if (res.code == 200) {
                        fnSuccess('操作成功！');
                        window.location = window.location;
                    } else {
                        alert(res.error || '取消失败')
                    }
                })
            }
        });
    //  材料准备阶段2-材料提交阶段3，国家审核中4-出证5/驳回6-完成8
        onClick($('.btn-begin'),function(){
            if(confirm('确认开始工作？')){
                $.post('/order/update',{
                    id:$search.id,
                    status:2,//
                },function(){
                     window.location.href =window.location.href;
                })
            }

        })
        onClick($('.btn-work'),function(){
            var $data = $(this).data('status');
            //debugger;
            if(confirm('确认'+orderStatus($data).text+'？')){
                $.post('/order/update',{
                    id:$search.id,
                    status:$data,//
                },function(){
                    window.location.href =window.location.href;
                })
            }

        })
        onClick($('.btn-send'), function () {// 标记发货
            if (confirm('确认标记订单为发货状态')) {
                $.post('/order/update', {id: $search.id, deliverStatus: '1'}, function () {
                    fnSuccess('操作成功！');
                    window.location = window.location;
                })
            }
        });
        onClick($('.btn-cancel-send'), function () {// 标记发货
            if (confirm('确认撤销订单的发货状态')) {
                $.post('/order/update', {id: $search.id, deliverStatus: '0'}, function () {
                    fnSuccess('操作成功！');
                    window.location = window.location;
                })
            }
        });

        $('.input-no').popover({
            title: '填入运单号',
            placement: 'top',
            html: true,
            content: '<div class="input-group"> \
                    <input type="text" class="form-control submit-no-val"  placeholder="请输入运单号"> \
                <span class="input-group-btn"> \
                <button class="btn btn-primary submit-no" type="button">提交</button> \
                </span> \
                </div>'
        });

        $('.input-no').on('shown.bs.popover', function () {
            onClick($('.submit-no'), function () {
                var $val = $('.submit-no-val').val();
                if (!$val)return alert('请输入运单号');
                $.post('/order/update', {
                    id: $search.id,
                    shipNo: $val,
                }, function () {
                    fnSuccess('提交成功！')
                    $('.input-no').popover('hide');
                    $('.input-no').hide();//
                    $('.input-no').hide();//
                    $('.input-back-no').show();
                    $('.label-shop-no').html($val);
                });
            });
        });
        // 撤销已填运单号
        onClick($('.input-back-no'), function () {
            if (confirm('确认撤销？')) {
                $.post('/order/update', {
                    id: $search.id,
                    shipNo: '',
                }, function () {
                    $('.input-no').show();
                    $('.input-back-no').hide();
                    $('.label-shop-no').html('');
                });
            }

        })
    })

})