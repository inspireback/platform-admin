$(function(){
	var self = this;
	self.editor = UE.getEditor('content', {
        //工具栏
        toolbars: [[
            'fullscreen','undo', 'redo', '|',
            'bold', 'italic', 'underline', 'strikethrough', , 'removeformat', 'formatmatch',
            '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist',  '|',
            'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|', 'indent', '|',
            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
            'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
            'simpleupload',
            'horizontal',
        ]]
        , lang: "zh-cn"
        //字体
        , 'fontfamily': [
            {label: '', name: 'songti', val: '宋体,SimSun'},
            {label: '', name: 'kaiti', val: '楷体,楷体_GB2312, SimKai'},
            {label: '', name: 'yahei', val: '微软雅黑,Microsoft YaHei'},
            {label: '', name: 'heiti', val: '黑体, SimHei'},
            {label: '', name: 'lishu', val: '隶书, SimLi'},
            {label: '', name: 'andaleMono', val: 'andale mono'},
            {label: '', name: 'arial', val: 'arial, helvetica,sans-serif'},
            {label: '', name: 'arialBlack', val: 'arial black,avant garde'},
            {label: '', name: 'comicSansMs', val: 'comic sans ms'},
            {label: '', name: 'impact', val: 'impact,chicago'},
            {label: '', name: 'timesNewRoman', val: 'times new roman'}
        ]
        //字号
        , 'fontsize': [10, 11, 12, 14, 16, 18, 20, 24, 36]
        , enableAutoSave: false
        , autoHeightEnabled: false
        , initialFrameHeight: '400px'
        , initialFrameWidth: '100%'
        , readonly: false
    });
    self.editor.ready(function (ueditor) {

    });

    self.editor.addListener('contentChange', function (editor_) {
     //   self.props.onchange(this.getContent())
    });
})