/**
 * Created by suweiming on 2017/12/10.
 */
var form = null, path = '', img_jcrop = null, searchObj = parseSearch();
$(function () {
    $('.upload-tips').click(function () {
        $(this).siblings('input').click();
    });
    $("#file1").unbind('change').on('change', function () {
        onchange();
    });

    onClick($('.save'), function () { // 保存提示
        if (!$('.preview-upload-image').find('img').length) {
            return fnFail('请上传文件');
        }
        $.post('/category/update', {
            url: $('.preview-upload-image').find('img').attr('src'),
            id: searchObj.id
        }, function (res) {
            if (res.code == 200) {
                fnSuccess('上传成功！')
                setTimeout(function(){
                    window.location.href = './index.html';
                },800)
            }
        })
    })
    function onchange() {
        ajaxFileUpload({
            eleId: 'file1'
        }, function (data) {
            $("#target").attr("src", data.data.path);
            $('.preview-container img').attr('src', data.data.path);
            form = null;
            if (!searchObj.noCrop) {
                path = data.data.path;
                if (img_jcrop && img_jcrop.destroy && img_jcrop.destroy instanceof 'function') {
                    img_jcrop.destroy();
                }
                $("#target").load(function () {
                    $('.jc-demo-box').show();
                    initJCrop();
                });
            } else {
                $('.preview-upload-image').html('<a target="_blank" href="' + data.data.path + '" title="新页面打开预览"><img src="' + data.data.path + '" style="width:100%;"></a>');
            }
            $("#file1").unbind('change').on('change', function () {
                onchange();
            });
        });
    }

    $('#save').click(function () {
        form.path = path;
        // $.post('/path-cut-image-oss', form, function (data) {
        $.post('/path-cut-image', form, function (data) {
            $.post('/goods/update', {id: searchObj.id, thumbnail: data.data.path}, function (data_) {
                $('.jc-demo-box').hide();
                fnSuccess && fnSuccess('保存成功!');
                $('#upload').attr('src', data.data.path);
            })
        });
    });
});
var boundx,boundy;
function initJCrop() {
    var jcrop_api,
        $preview = $('#preview-pane'),
        $pcnt = $('#preview-pane .preview-container'),
        $pimg = $('#preview-pane .preview-container img'),
        xsize = $pcnt.width(),
        ysize = $pcnt.height();
    img_jcrop = $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updatePreview,
        setSelect: [0, 0, 107, 142],
        aspectRatio: 107 / 142
    }, function () {
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        jcrop_api = this;
        $preview.appendTo(jcrop_api.ui.holder);
        transformNaturalSize({x: 0, y: 0, w: 107, h: 142}, boundx, boundy);
    });
    function updatePreview(c) {
        if (boundx) {
            transformNaturalSize(c, boundx, boundy);
        }
        if (parseInt(c.w) > 0) {
            var rx = xsize / c.w;
            var ry = ysize / c.h;
            $pimg.css({
                width: Math.round(rx * boundx) + 'px',
                height: Math.round(ry * boundy) + 'px',
                marginLeft: '-' + Math.round(rx * c.x) + 'px',
                marginTop: '-' + Math.round(ry * c.y) + 'px'
            });
        }
    };
    function transformNaturalSize(c, boundx, boundy) {
        var obj = getNatural();
        form = {};
        var xradio = obj.w / boundx;
        var yradio = obj.h / boundy;
        var radio = yradio > xradio ? yradio : xradio;

        form.w = Math.round(xradio * c.w);
        form.h = Math.round(yradio * c.h);
        form.y = Math.round(yradio * c.y);
        form.x = Math.round(xradio * c.x);
    }

    function getNatural() {
        var myimage = $('img[src="' + path + '"]')[0];
        if (typeof myimage.naturalWidth == "undefined") {
            // IE 6/7/8
            var i = new Image();
            i.src = path;
            return {
                w: i.width,
                h: i.height
            };
        }
        else {
            // HTML5 browsers
            var rw = myimage.naturalWidth;
            var rh = myimage.naturalHeight;
            return {
                w: rw,
                h: rh
            };
        }

    }
}
//# sourceMappingURL=../../../sys/scripts/sys/category/img.js.map
ripts/sys/category/img.js.map
