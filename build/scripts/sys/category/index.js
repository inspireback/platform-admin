/**
 * 弹框上传代码
 * Created by suweiming on 2018/1/19.
 */
(function ($) {
    var img_jcrop = '';

    function onchange(eleId, wh) {
        debugger;
        ajaxFileUpload({
            eleId: eleId,
            url: wh.url || '',
            data: wh.extraData || {}
        }, function (data) {
            if (!data) {
                $("#" + eleId).unbind('change').on('change', function () {
                    onchange(eleId, wh);
                });
                return;
            }
            if (wh.plain) {
                $("#" + eleId).unbind('change').on('change', function () {
                    onchange(eleId, wh);
                });
                return wh.submit(data.data);
            }
            $("#target").attr("src", data.data.path);
            $("#target").css('width', '100%');
            $("#target").data('fileId', data.data.fileId);
            $('.preview-container img').attr('src', data.data.path);
            if (wh.previewClass) {
                $('.' + wh.previewClass).attr('src', data.data.path);
            }

            if (wh.submitEachTime) {
                wh.submitEachTime(data.data.path);
            }

            form = null;
            if (!wh.noCrop) {

                path = data.data.path;
                if (img_jcrop && img_jcrop.destroy && img_jcrop.destroy instanceof 'function') {
                    img_jcrop.destroy();
                }
                $("#target").load(function () {
                    $('.jc-demo-box').show();
                    initJCrop(wh);
                });
            } else {
                $('.preview-upload-image').html('<a target="_blank" href="' + data.data.path + '" title="新页面打开预览"><img src="' + data.data.path + '" style="width:100%;"></a>');
                wh.submit(data.data);
            }
            $("#" + eleId).unbind('change').on('change', function () {
                onchange(eleId, wh);
            });
        });
    }

    var $Uploader = {
        init: function (options) {
            var $sel = $(this);
            $sel.parent().css({position: 'relative'});
            var $file = $('<input type="file" name="file" />');
            var $container = $('<div>');
            var $a = $('<a>');
            $a.html(options.label || ' 选 择 ');

            $a.addClass('btn btn-primary').css({
                margin: '10px 10px 0 0'
            });
            $container.append($file); //
            $container.append($a); //
            $container.css({'position': 'relative'}).addClass($sel.attr('class'));
            var $id = 'upload-' + parseInt(Math.random() * 10000);
            $file.attr('id', $id);
            $file.hide();
            $sel.replaceWith($container);
            onClick($a, function () {
                $("#" + $id).click();
            });
            $('#save').appendTo($a.parent());
            $("#" + $id).unbind('change').on('change', function () {
                onchange($id, options)
            });
        },
        initBtn: function () {
        }
    };
    $.fn.$upload = function (options) {
        var _default = {
            fields: []
        };
        options = $.extend({}, _default, options || {});
        $(this).html('<div>加载中...</div>');
        return this.each(function () {
            $Uploader.init.call(this, options);
        });
    };
    var boundx,
        boundy;
    // 裁剪功能
    $.$jCrop = initJCrop;

    function initJCrop(ratio) {
        if (ratio.path) {
            $("#target").attr("src", ratio.path);
            $("#target").css('width', '100%');
            $("#target").data('fileId', ratio.fileId);
            $('.preview-container img').attr('src', ratio.path);
            if (ratio.previewClass) {
                $('.' + wh.previewClass).attr('src', ratio.path);
            }
            path = ratio.path;
        }

        var jcrop_api,
            $preview = $('#preview-pane'),
            $pcnt = $('#preview-pane .preview-container'),
            $pimg = $('#preview-pane .preview-container img');

        $pcnt.show();
        $pcnt.css({
            width: ratio.w || 107,
            height: ratio.h || 142
        });
        var xsize = $pcnt.width(),
            ysize = $pcnt.height();
        img_jcrop = $('#target').Jcrop({
            onChange: updatePreview, // 拖动事件
            onSelect: updatePreview, // 选择事件
            setSelect: [0, 0, ratio.w || 107, ratio.h || 142], // 设置默认选择区
            aspectRatio: (ratio.w || 107) / (ratio.h || 142) // 设置选择宽高比例
        }, function () {
            // Use the API to get the real image size
            var bounds = this.getBounds();
            boundx = bounds[0];
            boundy = bounds[1];
            jcrop_api = this;
            // $preview.appendTo(jcrop_api.ui.holder);
            transformNaturalSize({x: 0, y: 0, w: ratio.w || 107, h: ratio.h || 142}, boundx, boundy);
        });
        onClick($('#save'), function () {
            form.path = path;
            if (!!$("#target").data('fileId')) {
                form.fileId = $("#target").data('fileId');
            }
            ;
            $.post('/path-cut-image', form, function (data) {
                $('.jc-demo-box').hide();
                fnSuccess && fnSuccess('保存成功!');
                ratio.submit && ratio.submit(data.data);
                //   $('#container_photo').html('<img style="width:100%;" src="' + data.data.path + '" />')
            });
        });
        function updatePreview(c) {
            if (boundx) {
                transformNaturalSize(c, boundx, boundy);
            }
            if (parseInt(c.w) > 0) {
                var rx = xsize / c.w;
                var ry = ysize / c.h;
                if (!boundy) {
                    boundy = $('#target').height();
                }
                $pimg.css({
                    width: 'auto',
                    // width: Math.round(rx * boundx) + 'px',
                    height: Math.round(ry * boundy) + 'px',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                });

            }
        };
        function transformNaturalSize(c, boundx, boundy) {
            var obj = getNatural();
            form = {};
            var xradio = obj.w / boundx;
            var yradio = obj.h / boundy;
            var radio = yradio > xradio ? yradio : xradio;

            form.w = Math.round(xradio * c.w);
            form.h = Math.round(yradio * c.h);
            form.y = Math.round(yradio * c.y);
            form.x = Math.round(xradio * c.x);
        }

        function getNatural() {
            var myimage = $('img[src="' + path + '"]')[0];
            if (typeof myimage.naturalWidth == "undefined") {
                // IE 6/7/8
                var i = new Image();
                i.src = path;
                return {
                    w: i.width,
                    h: i.height
                };
            }
            else {
                // HTML5 browsers
                var rw = myimage.naturalWidth;
                var rh = myimage.naturalHeight;
                return {
                    w: rw,
                    h: rh
                };
            }

        }
    }
})(jQuery);
function moreConstFn($search, search) {
    return {
        'type': { //
            query: '/const/' + ($search.type1 == 10 ? 'category' : 'dataR'),//分页查询URL,
            rendered: function () {
            },
            btns: [],
            beforeRender(data){
                return Object.keys(data).map(function (key) {
                    var item = data[key].split(':');
                    return {
                        key: key,
                        value: item[0],
                        label: item[1]
                    }
                })
            },
            destroyBtn: function () {
            },
            createBtn: function (_this) {
            },
            fields: [
                {name: 'key', label: 'KEY'},
                {name: 'value', label: '类型值'},
                {
                    name: 'label', label: '描述',
                    filter: function (item) {
                        return viewMore(item.label || ''); // 预览更多内容
                    }
                },
                {
                    label: '管理', filter: function (item) {
                    return '<a href="./index-v2.html?value=' + item.value + '&label='+item.label+'" >查看</a>'
                }
                },

            ],
        },
    };
};
$(function () {
    var $search = parseSearch(), arg = {}, tag = '';
    console.log($search);
    function operRes(res) {
        if (res.code == 200) {
            fnSuccess('操作成功');
            search({}, true);
        } else {
            fnFail(res.error || '操作失败');
        }
    }

    var diff = {
        category: { // 商品分类管理
            query: '/category/list?type1=shop-category&size=' + maxSize,//分页查询URL,
            tag: '分类管理',
            dateSearch: false,
            btns: [
                {
                    label: '增加',
                    class_: 'create',
                    onClick: function () {
                        var $index = dialogForm(
                            {
                                title: "增加分类",
                                data: [
                                    {
                                        type: 'text',
                                        label: '分类名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入分类名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '描述',
                                        placeholder: '请输入描述',
                                        name: 'desc1'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: '0',
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },

                                ],
                                submit: function (form) {
                                    form.level = 1;
                                    form.type1 = 'shop-category'; // 数据类型菜单
                                    $.post('/category/add', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '修改',
                    class_: 'update',
                    onClick: function () {
                        var $json = $(this).data('json');
                        if ($json.name == '批量上货导入') {
                            return alert('批量上货导入不能修改')
                        }
                        console.log($json);
                        var $index = dialogForm(
                            {
                                title: "修改商品分类",
                                data: [
                                    {
                                        type: 'text',
                                        label: '分类名称',
                                        val: $json.name,
                                        require: true, // 是否必填
                                        placeholder: '请输入分类名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '描述',
                                        val: $json.desc1 || '',
                                        placeholder: '请输入描述',
                                        name: 'desc1'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: $json.orderNum || 0,
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.id = $json.id; // 数据类型菜单
                                    $.post('/category/update', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },

                {
                    label: '删除',
                    class_: 'delete',
                    onClick: function () {
                        var $json = $(this).data('json');
                        if ($json.name == '批量上货导入') {
                            return alert('批量上货导入不能删除')
                        }
                        console.log($json);
                        if (confirm('确认删除？')) {
                            $.post('/category/delete/' + $json.id, {}, function (res) {
                                if (res.code == 200) {
                                    search({}, true);
                                    fnSuccess('删除成功！');
                                }
                            })
                        }
                    }
                },
                {
                    label: '增加二级分类',
                    onClick: function () {
                        var $jon = $(this).data('json');
                        console.log($json);
                        var $json = {};
                        if ($jon.level == 2) {
                            $json = $jon;
                        }
                        var $index = dialogForm(
                            {
                                title: "增加二级分类",
                                data: [
                                    {
                                        type: 'text',
                                        label: '二级分类',
                                        val: $json.name || '',
                                        require: true, // 是否必填
                                        placeholder: '请输入二级分类',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '描述',
                                        val: $json.desc1 || '',
                                        placeholder: '请输入描述',
                                        name: 'desc1'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号' || 0,
                                        val: $json.orderNum,
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                full: true,
                                submit: function (form) {
                                    form.level = 2;
                                    form.type1 = 'shop-category';
                                    if ($jon.level == 1) { //  为一级菜单增加子菜单
                                        form.pId = $jon.id;
                                    } else { // 修改子菜单
                                        form.id = $jon.id; // 数据类型菜单
                                    }
                                    //form.type1 = 'front-menu'; // 数据类型菜单
                                    $.post($jon.level == 1 ? '/category/add' : '/category/update', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '上架/下架',
                    class_: 'update',
                    onClick: function () {
                        var $jon = $(this).data('json');
                        if ($jon.name == '批量上货导入') {
                            return alert('批量上货导入不能上架')
                        }
                        var $json = {};
                        var ti = ['targetId=' + $jon.id, 'type1=shop-category', 'isShow=' + ($jon.isShow == '1' ? '0' : '1')];
                        $.get('/statistic1/set?' + ti.join('&'), function (res) {
                            if (res.code == 200) {
                                console.log('ok!');
                                fnSuccess(($jon.isShow == '1' ? '上架' : '下架') + '操作成功')
                                search({}, true);
                            }
                        })
                    }
                },
                {
                    label: '移动',
                    onClick: function () {
                        var $jon = $(this).data('json');
                        var $json = $jon, level = parseInt($jon.level);
                        var $index = dialogForm(
                            {
                                title: "移动",
                                data: [
                                    {
                                        type: 'text',
                                        label: '移动至',
                                        val: $json.name || '',
                                        require: true, // 是否必填
                                        placeholder: '请选择一级目录',
                                        name: 'pId'
                                    },
                                ],
                                full: true,
                                shown: function () {
                                    selectGoods({
                                        name: 'pId',
                                        url: '/category/list?type1=shop-category&level=' + (level - 1),
                                        selected: $json.pId,
                                        key: 'KEY' + Math.random()
                                    })
                                },
                                submit: function (form) {
                                    //$json.pId = form.pId;
                                    form.id = $json.id;
                                    $.post('/category/update', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("移动操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("移动操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '树形结构查看',
                    onClick: function () {
                        window.location.href = '../setting/category.html'
                    }
                },
                {
                    label: '分类图标',
                    class_: 'upload',
                    onClick: function () {

                        var $json = $(this).data('json');
                        imageSelector({
                            multi: false,
                            title: '',
                            body: $('#imgs').html()
                        }, {
                            ok: function (arr, cb, t) {
                                arr = arr || [];
                                arr.map(function (item) {
                                    initImg(item.url, false, item.id)
                                    cb(t)
                                })
                            },
                            cancel: function (cb, t) {
                                cb(t)
                            },
                            dbclick: function ($ele) {
                                $ele.find('img.appendImg').attr('src')
                                $ele.find('img.appendImg').attr('id')

                            },
                            click: function ($ele) {
                                $ele.find('img.appendImg').attr('src')
                                $ele.find('img.appendImg').attr('id')
                                var $id = $ele.find('img.appendImg').attr('id')
                                var $url = $ele.find('img.appendImg').attr('src')
                                if (!$url) {
                                    return;
                                }
                                $.post('/category/update', {id: $json.id, url: $url}, function (res) {
                                    if (res.code == 200) {
                                        search({}, true);
                                        fnSuccess('上传成功～')
                                    } else {
                                        fnFail(res.error || '上传失败！')
                                    }
                                })

                            }
                        })
                    },

                },
            ],
            destroyBtn: function () {
                disableBtn(btns[5]);
                disableBtn(btns[4]);
                disableBtn(btns[3]);
                disableBtn(btns[2]);
                disableBtn(btns[7]);
                disableBtn(btns[1]);
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                enableBtn(btns[1], $item);
                enableBtn(btns[2], $item);
                enableBtn(btns[4], $item);
                enableBtn(btns[7], $item);
                if ($item.level != '0') {
                    enableBtn(btns[5], $item);
                } else {
                    disableBtn(btns[5]);
                }
                if ($item.level == 1 && $item.type1 == 'shop-category') {
                    enableBtn(btns[3], $item);
                }
            },
            listFilter: function (res) {
                var t0 = _.filter(res, function (itme) {
                    return itme.level == 0;
                })
                var t = _.filter(res, function (itme) {
                    return itme.level == 1;
                })
                var tg = _.filter(res, function (itme) {
                    return itme.level == 2;
                });
                tg = _.groupBy(tg, 'pId');
                t = _.groupBy(t, 'pId');
                // t = _.sortBy(t, 'orderNum').reverse();
                // t = _.sortBy(t, 'orderNum').reverse();
                t0 = _.sortBy(t0, 'orderNum').reverse();
                t1 = [];
                t0.map(function (item0) {
                    t1.push(item0);
                    if (t[item0.id] && t[item0.id].length) {
                        t[item0.id].map(function (item) {
                            t1.push(item);
                            if (tg[item.id]) { //
                                t1.push(_.sortBy(tg[item.id], 'orderNum').reverse())
                            }
                        })
                    }
                })
                return _.flatten(t1, true);
            },
            fields: [
                {
                    name: 'name', label: '名称',
                    enableSearch: true,
                    fnStyle: function (item) {
                        return item.level == 2 ? ";padding-left:0px;" : '';
                    },
                    filter: function (item) {
                        if (item.level == 2) {
                            return item.level == 2 ? ('<div style="font-size: 14px;font-weight: 600;padding-left: 40px;margin-left: 40px;">' + item.name + '</div>') : '<div style="font-size: 14px;font-weight: 600;' + (item.name == '批量上货导入' ? 'color: red;' : '') + '">' + item.name + '</div>';
                        }
                        return item.level == 1 ? ('<div style="font-size: 14px;font-weight: 600;padding-left: 20px;margin-left: 20px;">' + item.name + '</div>') : '<div style="font-size: 14px;font-weight: 600;' + (item.name == '批量上货导入' ? 'color: red;' : '') + '">' + item.name + '</div>';
                    }
                },
                {
                    name: 'level', label: '层级',
                    filter: function (item) {
                        return item.level;
                    },
                    fnStyle: function (item) {
                        return item.level == 1 ? ";" : ''
                    }
                },
                {
                    name: 'isShow', label: '状态',
                    filter: function (item) {
                        return item.isShow == 1 ? "<span class='label label-success'>上架</span>" : "<span class='label label-warning'>下架</span>"
                    },
                },
                {
                    name: 'desc1', label: '描述',
                    fnStyle: function (item) {
                        if (item.level == 2) {
                            return ';padding-left:100px;';
                        }
                        return item.level == 1 ? ";padding-left:50px;" : ''
                    }
                },
                {
                    name: 'url', label: '分类图标',
                    filter: function (item) {
                        if (item.url) {
                            return '<a target="_blank" href="' + item.url + '" title="点击预览"><img style="height:60px;border-radius: 5px" src="' + item.url + '"></a>'
                        } else {
                            return getDefaultImgStr();
                        }
                    }
                },

                {
                    name: 'orderNum', label: '排序号',
                    fnStyle: function (item) {
                        if (item.level == 2) {
                            return ';padding-left:100px;';
                        }
                        return item.level == 1 ? ";padding-left:50px;" : ''
                    }
                },
            ],
        },
        'wild-swiper': { // 主题
            query: '/category/list?type1=' + $search.type,//分页查询URL,
            btns: [
                {
                    label: '增加',
                    class_: 'create',
                    onClick: function () {
                        var $index = dialogForm(
                            {
                                title: "增加",
                                data: [
                                    {
                                        type: 'text',
                                        label: '标题',
                                        require: true, // 是否必填
                                        placeholder: '请输入标题',
                                        name: 'name'
                                    },
                                    {
                                        type: 'textarea',
                                        label: '描述',
                                        val: '',
                                        require: true, // 是否必填
                                        placeholder: '请输入描述',
                                        name: 'desc1'
                                    },
                                    {
                                        type: 'text',
                                        label: '跳转URL',
                                        val: '',
                                        require: true, // 是否必填
                                        placeholder: '请输入跳转URL',
                                        name: 'remark'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: '0',
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.type1 = $search.type; // 数据类型菜单
                                    $.post('/category/add', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '修改',
                    class_: 'update',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        var $index = dialogForm(
                            {
                                title: "修改Banner",
                                data: [
                                    {
                                        type: 'text',
                                        label: '标题',
                                        val: $json.name,
                                        require: true, // 是否必填
                                        placeholder: '请输入标题',
                                        name: 'name'
                                    },
                                    {
                                        type: 'textarea',
                                        label: '描述',
                                        val: $json.desc1,
                                        require: true, // 是否必填
                                        placeholder: '请输入描述',
                                        name: 'desc1'
                                    },
                                    {
                                        type: 'text',
                                        label: '跳转URL',
                                        val: $json.remark || '',
                                        require: true, // 是否必填
                                        placeholder: '请输入跳转URL',
                                        name: 'remark'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: $json.orderNum,
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.id = $json.id; // 数据类型菜单
                                    form.type1 = $search.type; // 数据类型菜单
                                    $.post('/category/update', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '上传轮播图',
                    class_: 'upload',
                    onClick: function () {
                        var $json = $(this).data('json');
                        uploadFn('上传轮播图', {
                            w: $search.w || 400,
                            h: $search.h || 400,
                            num: 12,
                            labelNum: '12 upload-custom-label',
                            noCrop: !!$search.nocrop,
                            preview: 'left-top-preview'
                        }, function (data) {
                            closeLayer(data.layerIndex);
                            $.post('/category/update', {id: $json.id, url: data.path}, function (res) {
                                if (res.code == 200) {
                                    search({}, true);
                                    fnSuccess('上传成功～')
                                } else {
                                    fnFail(res.error || '上传失败！')
                                }
                            })
                            console.log(data);
                        });
                        // window.location.href = './img.html?id=' + $json.id + '&type=theme'
                    }
                },
                {
                    label: '删除',
                    class_: 'delete',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        if (confirm('确认删除？')) {
                            $.post('/category/delete/' + $json.id, {}, function (res) {
                                if (res.code == 200) {
                                    search({}, true);
                                    fnSuccess('删除成功！');
                                }
                            })
                        }
                    }
                },

                {
                    label: '上架/下架',
                    class_: 'update',
                    onClick: function () {
                        var $jon = $(this).data('json');
                        var $json = {};
                        var ti = ['targetId=' + $jon.id, 'type1=' + $search.type, 'isShow=' + ($jon.isShow == '1' ? '0' : '1')];
                        $.get('/statistic1/set?' + ti.join('&'), function (res) {
                            if (res.code == 200) {
                                console.log('ok!');
                                search({}, true);
                            }
                        })
                    }
                },
            ],
            destroyBtn: function () {
                disableBtn(btns[4]);
                disableBtn(btns[3]);
                disableBtn(btns[2]);
                disableBtn(btns[1]);
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                enableBtn(btns[1], $item);
                enableBtn(btns[2], $item);
                enableBtn(btns[3], $item);
                enableBtn(btns[4], $item);
            },
            fields: [
                {name: 'name', label: '标题', enableSearch: true},
                {
                    name: 'desc1', label: '描述', filter: function (item) {
                    return viewMore(item.desc1 || ''); // 预览更多内容
                }
                },
                {
                    name: 'url', label: '跳转URL', filter: function (item) {
                    if (item.remark) {
                        return '<a target="_blank"  href="' + item.remark + '" title="点击预览">' + item.remark + '</a>'
                    } else {
                        return getDefaultImgStr();
                    }
                }
                },
                {
                    name: 'isShow', label: '上架状态',
                    filter: function (item) {
                        return item.isShow == 1 ? "<span class='label label-success'>上架</span>" : "<span class='label label-warning'>下架</span>"
                    },
                },
                {name: 'orderNum', label: '排序号'},
                {
                    name: 'url', label: '轮播图',
                    filter: function (item) {
                        if (item.url) {
                            return '<a target="_blank" style="display:inline-block;" href="' + item.url + '" title="点击预览"><img style="height:60px;border-radius: 5px" src="' + item.url + '"></a>'
                        } else {
                            return getDefaultImgStr();
                        }
                    },
                }
            ],
        },
        'wild-category': { // 多分类匹配
            query: '/category/list?type1=' + $search.type,//分页查询URL,
            rendered: function () {
                if ($search.rmBtn) {
                    $search.rmBtn.split(',').map(function (btn) {
                        if (btn == 'add') {
                            btns[0].remove();
                        }
                        if (btn == 'rm') {
                            btns[2].remove();
                        }
                    })
                }
                if (!!$search.link) {
                    btns[5].html($search.link);
                } else {
                    btns[5].remove();
                }

            },
            btns: [
                {
                    label: '增加',
                    class_: 'create',
                    onClick: function () {
                        var $index = dialogForm(
                            {
                                title: "增加",
                                data: [
                                    {
                                        type: 'text',
                                        label: '名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'textarea',
                                        label: '描述',
                                        val: '',
                                        require: true, // 是否必填
                                        placeholder: '请输入描述',
                                        name: 'desc1'
                                    },

                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: '0',
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.type1 = $search.type; // 数据类型菜单
                                    $.post('/category/add', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '修改',
                    class_: 'update',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        var $index = dialogForm(
                            {
                                title: "修改",
                                data: [
                                    {
                                        type: 'text',
                                        label: '名称',
                                        val: $json.name,
                                        require: true, // 是否必填
                                        placeholder: '请输入名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'textarea',
                                        label: '描述',
                                        val: $json.desc1,
                                        require: true, // 是否必填
                                        placeholder: '请输入描述',
                                        name: 'desc1'
                                    },

                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: $json.orderNum,
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.id = $json.id; // 数据类型菜单
                                    form.type1 = $search.type; // 数据类型菜单
                                    $.post('/category/update', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '删除',
                    class_: 'delete',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        if (confirm('确认删除？')) {
                            $.post('/category/delete/' + $json.id, {}, function (res) {
                                if (res.code == 200) {
                                    search({}, true);
                                    fnSuccess('删除成功！');
                                }
                            })
                        }
                    }
                },
                {
                    label: '上架/下架',
                    class_: 'update',
                    onClick: function () {
                        var $jon = $(this).data('json');
                        var $json = {};
                        var ti = ['targetId=' + $jon.id, 'type1=' + ($search.type), 'isShow=' + ($jon.isShow == '1' ? '0' : '1')];
                        $.get('/statistic1/set?' + ti.join('&'), function (res) {
                            if (res.code == 200) {
                                console.log('ok!');
                                search({}, true);
                            }
                        })
                    }
                },
                {
                    label: '图片选择器',
                    class_: 'upload select-from-libs',
                    // onClick: function () {
                    //     var $json = $(this).data('json');
                    //     var $json = $(this).data('json');
                    //     uploadFn('上传图片', {
                    //         w: $search.w || 100,
                    //         h: $search.h || 100,
                    //         num: 12,
                    //         labelNum: 12 + ' upload-custom-label',
                    //         noCrop: !!$search.nocrop,
                    //         preview: 'left-top-preview'
                    //     }, function (data) {
                    //         closeLayer(data.layerIndex);
                    //         $.post('/category/update', {id: $json.id, url: data.path}, function (res) {
                    //             if (res.code == 200) {
                    //                 fnSuccess('上传成功～');
                    //                 search({}, true)
                    //             } else {
                    //                 fnFail(res.error || '上传失败！')
                    //             }
                    //         })
                    //         console.log(data);
                    //     });
                    // }
                    onClick:function () {
                        imageSelector({
                            multi: true,
                            title: '',
                            body: $('#imgs').html()
                        }, {
                            ok: function (arr, cb, t) {
                                arr = arr || [];
                                arr.map(function (item) {
                                    initImg(item.url, false, item.id)
                                    cb(t)
                                })
                            },
                            cancel: function (cb, t) {
                                cb(t)
                            },
                            dbclick: function ($ele) {
                                $ele.find('img.appendImg').attr('src')
                                $ele.find('img.appendImg').attr('id')

                            },
                            click: function ($ele) {
                                $ele.find('img.appendImg').attr('src')
                                $ele.find('img.appendImg').attr('id')
                            }
                        })
                    }
                },
                {
                    label: '关联',
                    _class: 'link',
                    onClick: function () {
                        var $json = $(this).data('json');
                        window.location.href = './index.html?type=' + $search.type + '-' + $json.id + '&w=335&h=200&rmBtn=rm'
                    }
                }
            ],
            destroyBtn: function () {
                disableBtn(btns[3]);
                disableBtn(btns[2]);
                disableBtn(btns[1]);
                disableBtn(btns[4]);
                if (!!$search.link) {
                    disableBtn(btns[5]);
                }
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                enableBtn(btns[1], $item);
                enableBtn(btns[2], $item);
                enableBtn(btns[3], $item);
                enableBtn(btns[4], $item);
                if (!!$search.link) {//
                    enableBtn(btns[5], $item);
                }
            },
            fields: [
                {name: 'name', label: '名称', enableSearch: true},
                {
                    name: 'desc1', label: '描述',
                    filter: function (item) {
                        return viewMore(item.desc1 || ''); // 预览更多内容
                    }
                },
                {
                    name: 'desc1', label: '图片',
                    filter: function (item) {
                        if (item.url) {
                            return '<a target="_blank" style="display:inline-block;" href="' + item.url + '" title="点击预览"><img style="height:60px;border-radius: 5px" src="' + item.url + '"></a>'
                        } else {
                            return getDefaultImgStr();
                        }
                    }
                },
                {
                    name: 'isShow', label: '上架状态',
                    filter: function (item) {
                        return item.isShow == 1 ? "<span class='label label-success'>上架</span>" : "<span class='label label-warning'>下架</span>"
                    },
                },
                {name: 'orderNum', label: '排序号'},
            ],
        },
        'commission-withdraw': { // 多分类匹配
            query: '/userWx/history/commission?type1=commission-withdraw',//分页查询URL,
            rendered: function () {
            },
            btns: [
                {
                    label: '通过', onClick: function () {
                    var $json = $(this).data('json');
                    if (confirm('确认审核通过？')) {
                        $.post('/category/update', {
                            id: $json.pId,
                            status: 3
                        }, function (res) {
                            if (res.code == 200) {
                                fnSuccess('操作成功')
                            } else {
                                fnFail('操作失败，请稍后重试～')
                            }
                        })
                    }
                }
                },
                {
                    label: '拒绝', onClick: function () {
                    var $json = $(this).data('json');
                    if (confirm('确认拒绝通过？')) {
                        $.post('/category/update', {
                            id: $json.id,
                            status: 2
                        }, function (res) {
                            if (res.code == 200) {
                                fnSuccess('操作成功')
                            } else {
                                fnFail('操作失败，请稍后重试～')
                            }
                        })
                    }
                }
                },
                {
                    label: '打款', onClick: function () {
                    var $json = $(this).data('json');
                    if (confirm('确认拒绝通过？')) {
                        $.post('/category/update', {
                            id: $json.id,
                            status: 3
                        }, function (res) {
                            if (res.code == 200) {
                                fnSuccess('操作成功')
                            } else {
                                fnFail('操作失败，请稍后重试～')
                            }
                        })
                    }
                }
                },
            ],
            destroyBtn: function () {
                disableBtn(btns[0]);
                disableBtn(btns[1]);
                disableBtn(btns[2]);

            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                if ($item.status == 3) {
                    enableBtn(btns[2], $item);
                }
                if ($item.status == 1) {
                    enableBtn(btns[0], $item);
                    enableBtn(btns[1], $item);
                }
            },
            rendered: function () {
                onClick($('.detail'), function () {  //detail
                    //alert('')
                    $.get('/commission/list?saleId=' + $(this).data('id'), function (user) {
                        layerAlert({
                            full: true,
                            title: '佣金' + $(this).text(),
                            body: ejs.render($('#commission-list').html(), user),
                            shown: function () {
                                var _$table = $('.commission-table .table');
                                destroyBtn();
                                _$table.$table($.extend(TABLE_SETTING, {
                                    checkbox: true, // 是否显示复选框
                                    seqNum: true, // 是否显示序号
                                    toolbar: '.commission-toolbar',
                                    showSearchComplex: true,
                                    rendered: function () {
                                    },
                                    fields: [
                                        {name: 'no', label: '编号(订单号)'},
                                        {name: 'name', label: 'label'},
                                        {name: 'commission', label: '金额'},
                                        {
                                            name: 'type1', label: '佣金类型', filter: function (item) {
                                            if (1011 == item.type1)return '0元软著分享红包';
                                            if (1012 == item.type1)return '高新技术企业分享红包';
                                            if (1013 == item.type1)return '双软分享红包';
                                            if (1113 == item.type1)return '合伙人佣金';
                                            return '';
                                        }
                                        },
                                        {name: 'status', label: '状态'},
                                        {
                                            name: 'createAt', label: '获取时间', filter: function (item) {
                                            return formatTime(item.createAt);
                                        }
                                        }

                                    ],
                                    data: user.data
                                }));
                            }
                        })
                    })
                })
            },
            fields: [
                {
                    name: 'name',
                    label: '用户名称',
                    enableSearch: true,
                    filter: function (item) {
                        return '<a href="javascript:void(0)" data-id="' + item.pId + '" class="detail">' + item.name + '</a>'
                    }
                },
                {
                    name: 'phone',
                    label: '手机号',
                },
                {
                    name: 'status', label: '状态',
                    filter: function (item) {
                        return generateWithDrawStatusLabel(item.status);
                    }
                },
                {name: 'total', label: '提现金额',},
                {
                    name: 'remark',
                    label: '申请时间',
                    filter: function (item) {
                        return formatTime(parseInt(item.remark));
                    }
                },
            ],
        },
        order: { // 订单管理
            query: '/order/query',//分页查
            rendered: function () {

            },
            fields: [
                {name: '', label: '活动标题'},
                {
                    name: 'desc1', label: '描述', filter: function (item) {
                    return viewMore(item.desc1 || ''); // 预览更多内容
                }
                },
                {name: 'orderNum', label: '排序号'},

            ],
        },
        "wild-article": {
            query: '/info/queryWithCategory?type1=' + $search.type1 + '&cType=' + $search.type,//,
            complexItems: [],
            tag: '分类',
            btns: [
                {
                    label: '新建',
                    onClick: function () {
                        return window.location.href = './edit.html?type=' + $search.type + '&type1=' + $search.type1
                    }
                },
                {
                    label: '修改',
                    class_: 'update',
                    onClick: function () {
                        var $json = $(this).data('json');
                        return window.location.href = './edit.html?type=' + $search.type + '&id=' + $json.id + '&type1=' + $search.type1;
                    }
                },

                {
                    label: '删除',
                    class_: 'delete',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        if (confirm('确认删除？')) {
                            $.post('/info/delete/', {id: $json.id}, function (res) {
                                if (res.code == 200) {
                                    search({}, true);
                                    fnSuccess('删除成功！');
                                }
                            })
                        }
                    }
                },
                {
                    label: '发布<=>草稿',
                    onClick: function () {
                        var $json = $(this).data('json');
                        var label = $json.status == '1' ? '草稿' : ($json.status == '0' ? '发布' : (''));
                        if (confirm('确认' + label + '？')) {
                            $.post('/info/update', {
                                id: $json.id,
                                status: $json.status == '1' ? 0 : '1',
                            }, function (res) {
                                if (res.code == 200) {
                                    fnSuccess(label + '操作成功');
                                    search({}, true);
                                } else {
                                    fnFail(label + '操作失败');
                                }
                            })
                        }
                    }
                },
                {
                    label: '分类编辑',
                    class_: 'btn-info',
                    onClick: function () {
                        window.location.href = './index.html?type=' + $search.type + '-category'
                    }
                },
            ],
            destroyBtn: function () {
                disableBtn(btns[1]);
                disableBtn(btns[2]);
                disableBtn(btns[3]);
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                if ($item.status != 1) { //发布状态不能修改、删除
                    enableBtn(btns[1], $item);
                    enableBtn(btns[2], $item);
                }
                enableBtn(btns[3], $item);
            },
            rendered: function () {
                if ($search.rmBtn) {
                    $search.rmBtn.split(',').map(function (btn) {
                        if (btn == 'add') {
                            btns[0].remove();
                        }
                        if (btn == 'rm') {
                            btns[2].remove();
                        }
                    })
                }
                onClick($('.detail'), function () {
                    var ind = layer.open({
                        type: 2,
                        title: $(this).html(),
                        shadeClose: true,
                        shade: false,
                        maxmin: true, //开启最大化最小化按钮
                        fullScreen: true,
                        content: $(this).data('href')
                    });
                    layer.full(ind);


                })
            },
            fields: [
                {
                    name: 'title', label: '标题', enableSearch: true, filter: function (item) {
                    return '<a  href="javascript:void(0)" class="detail"  data-href="../../product/info/detail.html?noHead=1&id=' + item.id + '">' + item.title + '</a>';
                }
                },
                {name: 'category', label: '分类'},
                {
                    name: 'summary', label: '简介',
                    filter: function (item) {
                        return viewMore(item.summary);
                    }
                },
                {
                    name: 'thumbnail', label: '预览图', filter: function (item) {
                    if (!item.thumbnail)return '无';
                    return '<a target="_blank" style="display:inline-block;" href="' + item.thumbnail + '" title="点击预览"><img style="height:60px;border-radius: 5px" src="' + item.thumbnail + '"></a>'

                }
                },
                {
                    name: 'createAt', label: '发布时间',
                    filter: function (item) {
                        return formatTime(item.createAt);
                    }
                },
                {
                    name: 'status', _sort: 'status', label: '状态', filter: function (item) {
                    return (item.status == 1 ? '<span class="label label-success">发布</span>' : (item.status == 0 ? '<span class="label label-danger">草稿</span>' : (item.status == 0 ? '删除' : '')) );
                }
                }

            ],
        },
        permission: {
            query: '/role/getPermission/3?type1=info',//分页查询URL,
            tag: '权限',
            btns: [
                {
                    label: '增加',
                    class_: 'create',
                    onClick: function () {
                        var $index = dialogForm(
                            {
                                title: "增加权限",
                                data: [
                                    {
                                        type: 'text',
                                        label: '权限名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入权限名称',
                                        name: 'permissionName'
                                    },
                                    {
                                        type: 'text',
                                        label: '访问路径',
                                        require: true, // 是否必填
                                        placeholder: '请输入访问路径',
                                        name: 'url'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: '0',
                                        placeholder: '请输入排序号',
                                        name: 'remark'
                                    },
                                ],
                                submit: function (form) {
                                    form.type = 1;
                                    $.post('/role/add/permission', form, function (res) {
                                        if (res.code == 0) {
                                            fnSuccess("操作成功,下次登录后生效！");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '修改',
                    class_: 'update',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        var $index = dialogForm(
                            {
                                title: "修改权限",
                                data: [
                                    {
                                        type: 'text',
                                        label: '权限名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入权限名称',
                                        val: $json.permissionName,
                                        name: 'permissionName'
                                    },
                                    {
                                        type: 'text',
                                        label: '访问路径',
                                        require: true, // 是否必填
                                        placeholder: '请输入访问路径',
                                        name: 'url',
                                        val: $json.url,
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: $json.remark,
                                        placeholder: '请输入排序号',
                                        name: 'remark',
                                    },
                                ],
                                submit: function (form) {
                                    form.id = $json.id;
                                    $.post('/role/update/permission', form, function (res) {
                                        if (res.code == 0) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '删除',
                    onClick: function () {
                        var $json = $(this).data('json');
                        $.post('/role/delete/permission', {
                            id: $json.id
                        }, function (res) {
                            if (res.code == 0) {
                                fnSuccess("操作成功");
                                search({}, true);
                                closeLayer($index);
                            } else {
                                fnFail("操作失败");
                            }
                        })
                    }
                }
            ],
            destroyBtn: function () {
                disableBtn(btns[1]);
                disableBtn(btns[2]);
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                enableBtn(btns[1], $item);
                enableBtn(btns[2], $item);
            },
            fields: [
                {name: 'permissionName', label: '权限名称'},
                {name: 'description', label: '描述'},
                {name: 'url', label: 'URL'},
                {name: 'remark', label: '排序号'},
            ],
        },
        msg: {
            query: '/pLetter/list',//分页查询URL,
            tag: '系统消息',
            btns: [
                {
                    label: '标记为已读',
                    class_: 'delete',
                    onClick: function () {
                        var $json = $(this).data('json');
                        $.post('/pLetter/update', {id: $json.id, readStatus: 1}, function (res) {
                            if (res.code == 200) {
                                fnSuccess("操作成功");
                                search({}, true);
                            } else {
                                fnFail("操作失败");
                            }
                        })
                    }
                },
                {
                    label: '删除',
                    class_: 'delete',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        if (confirm("确认删除?")) {
                            $.post('/pLetter/delete/' + $json.id, {}, function (res) {
                                if (res.code == 200) {
                                    fnSuccess("操作成功");
                                    search({}, true);
                                } else {
                                    fnFail("操作失败");
                                }
                            })
                        }
                    }
                }
            ],
            destroyBtn: function () {
                disableBtn(btns[0]);
                disableBtn(btns[1]);
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                enableBtn(btns[0], $item);
                enableBtn(btns[1], $item);
            },
            rendered: function () {
            },
            fields: [
                {
                    name: 'readStatus', label: '状态', filter: function (item) {
                    return item.readStatus == 0 ? '【<a href="javascript:void(0);">未读</a>】' : '【<a href="javascript:void(0);">已读</a>】'
                }
                },
                {name: 'toUser', label: '发件人'},
                {name: 'remark', label: '类型'},
                {name: 'content', label: '内容'},
                {
                    name: 'createAt', label: '时间',
                    filter: function (item) {
                        return formatTime(item.createAt);
                    }
                },
            ],
        },
        menu: {
            query: '/category/list?type1=fi&size=150',//分页查询URL,
            tag: '菜单配置',
            btns: [
                {
                    label: '增加',
                    class_: 'create',
                    onClick: function () {
                        var $index = dialogForm(
                            {
                                title: "增加前端菜单",
                                data: [
                                    {
                                        type: 'text',
                                        label: '菜单名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入菜单名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '英文名称',
                                        val: '',
                                        require: true, // 是否必填
                                        placeholder: '请输入英文名称',
                                        name: 'en'
                                    },
                                    {
                                        type: 'text',
                                        label: '访问链接',
                                        val: '',
                                        require: true, // 是否必填
                                        placeholder: '请输入访问链接',
                                        name: 'remark'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: '0',
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.level = 1;
                                    form.type1 = 'front-menu'; // 数据类型菜单
                                    $.post('/category/add', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '修改',
                    class_: 'update',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        var $index = dialogForm(
                            {
                                title: "修改前端菜单",
                                data: [
                                    {
                                        type: 'text',
                                        label: '菜单名称',
                                        val: $json.name,
                                        require: true, // 是否必填
                                        placeholder: '请输入菜单名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '英文名称',
                                        val: $json.en,
                                        require: true, // 是否必填
                                        placeholder: '请输入英文名称',
                                        name: 'en'
                                    },
                                    {
                                        type: 'text',
                                        label: '访问链接',
                                        val: $json.remark || '',
                                        require: true, // 是否必填
                                        placeholder: '请输入访问链接',
                                        name: 'remark'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: $json.orderNum,
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.id = $json.id; // 数据类型菜单
                                    //form.type1 = 'front-menu'; // 数据类型菜单
                                    $.post('/category/update', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '上传',
                    class_: 'upload',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        window.location.href = './img.html?id=' + $json.id + '&noCrop=1'

                    }
                },
                {
                    label: '删除',
                    class_: 'delete',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        if (confirm('确认删除？')) {
                            $.post('/category/delete/' + $json.id, {}, function (res) {
                                if (res.code == 200) {
                                    search({}, true);
                                    fnSuccess('删除成功！');
                                }
                            })
                        }
                    }
                },
                {
                    label: '增加二级菜单',
                    class_: 'update',
                    onClick: function () {
                        var $jon = $(this).data('json');
                        var $json = {};
                        if ($jon.level == 2) {
                            $json = $jon;
                        }
                        var $index = dialogForm(
                            {
                                title: "增加二级菜单",
                                data: [
                                    {
                                        type: 'text',
                                        label: '二级菜单名称',
                                        val: $json.name || '',
                                        require: true, // 是否必填
                                        placeholder: '请输入二级菜单名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '英文名称',
                                        val: $json.en || '',
                                        require: true, // 是否必填
                                        placeholder: '请输入英文名称',
                                        name: 'en'
                                    },
                                    {
                                        type: 'text',
                                        label: '访问链接',
                                        val: $json.remark || '',
                                        require: true, // 是否必填
                                        placeholder: '请输入访问链接',
                                        name: 'remark'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号' || 0,
                                        val: $json.orderNum,
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.level = 2;
                                    form.type1 = 'front-menu';
                                    if ($jon.level == 1) { //  为一级菜单增加子菜单
                                        form.pId = $jon.id;
                                    } else { // 修改子菜单
                                        form.id = $jon.id; // 数据类型菜单
                                    }
                                    //form.type1 = 'front-menu'; // 数据类型菜单
                                    $.post($jon.level == 1 ? '/category/add' : '/category/update', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '导航中显示/隐藏',
                    class_: 'update',
                    onClick: function () {
                        var $jon = $(this).data('json');
                        var $json = {};
                        var ti = ['targetId=' + $jon.id, 'type1=category', 'isShow=' + ($jon.isShow == '1' ? '0' : '1')];
                        $.get('/statistic1/set?' + ti.join('&'), function (res) {
                            if (res.code == 200) {
                                console.log('ok!');
                                search({}, true);
                            }
                        })
                    }
                }
            ],
            destroyBtn: function () {
                disableBtn(btns[5]);
                disableBtn(btns[4]);
                disableBtn(btns[3]);
                disableBtn(btns[2]);
                disableBtn(btns[1]);
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                enableBtn(btns[1], $item);

                if ($item.name != '首页') {
                    enableBtn(btns[5], $item);
                    enableBtn(btns[3], $item);
                    enableBtn(btns[2], $item);
                }

                if ($item.name != '首页' && $item.level == 1 && $item.type1 == 'front-menu') {
                    enableBtn(btns[4], $item);
                }
            },
            listFilter: function (res) {
                console.log(res);
                var t = _.filter(res, function (itme) {
                    return itme.level == 1;
                })
                var tg = _.filter(res, function (itme) {
                    return itme.level == 2;
                });
                tg = _.groupBy(tg, 'pId');
                t = _.sortBy(t, 'orderNum').reverse();
                t1 = [];
                t.map(function (item) {
                    t1.push(item);
                    if (tg[item.id]) { //
                        t1.push(_.sortBy(tg[item.id], 'orderNum').reverse())
                    }
                })
                return _.flatten(t1, true);
            },
            fields: [
                {
                    name: 'name', label: '名称',
                    fnStyle: function (item) {
                        return item.level == 2 ? "background:aliceblue;padding-left:0px;" : ''
                    },
                    filter: function (item) {
                        return item.level == 2 ? ('<div style="border-left: 2px solid #c7c3c3;padding-left: 20px;margin-left: 20px;">' + item.name + '</div>') : item.name;
                    }
                },
                {
                    name: 'level', label: '层级',
                    filter: function (item) {
                        return item.level;
                    },
                    fnStyle: function (item) {
                        return item.level == 2 ? "background:aliceblue;padding-left:50px;" : ''
                    }
                },
                {
                    name: 'isShow', label: '是否在导航中显示',
                    filter: function (item) {
                        return item.isShow == 1 ? "显示" : '不显示'
                    },
                },
                {
                    name: 'en', label: '英文名',
                    fnStyle: function (item) {
                        return item.level == 2 ? "background:aliceblue;padding-left:50px;" : ''
                    }
                },
                {
                    name: 'desc1', label: '描述',
                    fnStyle: function (item) {
                        return item.level == 2 ? "background:aliceblue;padding-left:50px;" : ''
                    }
                },
                {
                    name: 'orderNum', label: '排序号',
                    fnStyle: function (item) {
                        return item.level == 2 ? "background:aliceblue;padding-left:50px;" : ''
                    }
                },
                {
                    name: 'url', label: '访问链接',
                    filter: function (item) {
                        if (item.remark) {
                            return '<a target="_blank" href="' + item.remark + '" title="点击预览">' + item.remark + '</a>'
                        } else {
                            return getDefaultImgStr();
                        }
                    },
                    fnStyle: function (item) {
                        return item.level == 2 ? "background:aliceblue;padding-left:50px;" : ''
                    }
                },
                {
                    name: 'url', label: '封面图',
                    filter: function (item) {
                        if (item.url) {
                            return '<a target="_blank" href="' + item.url + '" title="点击预览">' + item.url + '</a>'
                        } else {
                            return getDefaultImgStr();
                        }
                    },
                    fnStyle: function (item) {
                        return item.level == 2 ? "background:aliceblue;padding-left:50px;" : ''
                    }
                },
            ],
        },
        brand: { // 商品品牌
            query: '/category/list?type1=brand',//分页查询URL,
            tag: '分类',
            btns: [
                {
                    label: '增加',
                    class_: 'create',
                    onClick: function () {
                        var $index = dialogForm(
                            {
                                title: "添加品牌",
                                data: [
                                    {
                                        type: 'text',
                                        label: '品牌名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入品牌名称',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '品牌英文名称',
                                        val: '',
                                        require: true, // 是否必填
                                        placeholder: '请输入品牌英文名称',
                                        name: 'en'
                                    },
                                    {
                                        type: 'textarea',
                                        label: '品牌简介',
                                        val: '',
                                        require: true, // 是否必填
                                        placeholder: '请输入品牌简介',
                                        name: 'desc1'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: '0',
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.level = 1;
                                    form.type1 = 'brand'; // 数据类型菜单
                                    if (!form.name)return alert('请输入品牌名称');
                                    if (!form.en)return alert('请输入品牌英文名称');
                                    $.post('/category/add', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '修改',
                    class_: 'update',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        var $index = dialogForm(
                            {
                                title: "修改品牌信息",
                                data: [
                                    {
                                        type: 'text',
                                        label: '品牌名称',
                                        require: true, // 是否必填
                                        placeholder: '请输入品牌名称',
                                        val: $json.name || '',
                                        name: 'name'
                                    },
                                    {
                                        type: 'text',
                                        label: '品牌英文名称',
                                        val: $json.en || '',
                                        require: true, // 是否必填
                                        placeholder: '请输入品牌英文名称',
                                        name: 'en'
                                    },
                                    {
                                        type: 'textarea',
                                        label: '品牌简介',
                                        val: $json.desc1 || '',
                                        require: true, // 是否必填
                                        placeholder: '请输入品牌简介',
                                        name: 'desc1'
                                    },
                                    {
                                        type: 'text',
                                        label: '排序号',
                                        val: $json.orderNum || 0,
                                        placeholder: '请输入排序号',
                                        name: 'orderNum'
                                    },
                                ],
                                submit: function (form) {
                                    form.id = $json.id; // 数据类型菜单
                                    if (!form.name)return alert('请输入品牌名称');
                                    if (!form.en)return alert('请输入品牌英文名称');
                                    $.post('/category/update', form, function (res) {
                                        if (res.code == 200) {
                                            fnSuccess("操作成功");
                                            search({}, true);
                                            closeLayer($index);
                                        } else {
                                            fnFail("操作失败");
                                        }
                                    })
                                }
                            });
                    }
                },
                {
                    label: '删除',
                    class_: 'delete',
                    onClick: function () {
                        var $json = $(this).data('json');
                        console.log($json);
                        if (confirm('确认删除？')) {
                            $.post('/category/delete/' + $json.id, {}, function (res) {
                                if (res.code == 200) {
                                    search({}, true);
                                    fnSuccess('删除成功！');
                                }
                            })
                        }
                    }
                },
                {
                    label: '海报',
                    class_: 'upload',
                    onClick: function () {
                        var $json = $(this).data('json');
                        imageSelector({
                            multi: false,
                            title: '',
                            body: $('#imgs').html()
                        }, {
                            ok: function (arr, cb, t) {
                                arr = arr || [];
                                arr.map(function (item) {
                                    initImg(item.url, false, item.id)
                                    cb(t)
                                })
                            },
                            cancel: function (cb, t) {
                                cb(t)
                            },
                            dbclick: function ($ele) {
                                debugger;
                                $ele.find('img.appendImg').attr('src')
                                $ele.find('img.appendImg').attr('id')

                            },
                            click: function ($ele) {
                                debugger;
                                $ele.find('img.appendImg').attr('src')
                                $ele.find('img.appendImg').attr('id')
                                var $id = $ele.find('img.appendImg').attr('id')
                                var $url = $ele.find('img.appendImg').attr('src')
                                if (!$url) {
                                    return;
                                }
                                $.post('/category/update', {id: $json.id, url: $url}, function (res) {
                                    if (res.code == 200) {
                                        search({}, true);
                                        fnSuccess('上传成功～')
                                    } else {
                                        fnFail(res.error || '上传失败！')
                                    }
                                })

                            }
                        })

                    }
                },
                {
                    label: '上架/下架',
                    onClick: function () {
                        var $jon = $(this).data('json');
                        var $json = {};
                        var status = ($jon.isShow == '1' ? '0' : '1');
                        if (status == '1') {
                            if (!$jon.en || !$jon.desc1 || !$jon.url) {
                                return alert('上架品牌，英文名称、描述、海报任一项都不能为空～')
                            }
                        }
                        var ti = ['targetId=' + $jon.id, 'type1=brand', 'isShow=' + status];
                        $.get('/statistic1/set?' + ti.join('&'), function (res) {
                            if (res.code == 200) {
                                alert((status == '1' ? '上架' : "下架") + '成功');
                                search({}, true);
                            }
                        })
                    }
                },
            ],
            destroyBtn: function () {
                disableBtn(btns[1]);
                disableBtn(btns[2]);
                disableBtn(btns[3]);
                disableBtn(btns[4]);
            },
            createBtn: function (_this) {
                var $item = $(_this).data('json');
                enableBtn(btns[1], $item);
                enableBtn(btns[2], $item);
                enableBtn(btns[3], $item);
                enableBtn(btns[4], $item);
            },
            fields: [
                {name: 'name', label: '名称', enableSearch: true,},
                {name: 'en', label: '英文名'},
                {
                    name: 'desc1', label: '描述',
                    filter: function (item) {
                        return viewMore(item.desc1 || ''); // 预览更多内容
                    }
                },
                {name: 'orderNum', label: '排序号'},
                {
                    name: 'isShow', label: '状态',
                    filter: function (item) {
                        return item.isShow == '1' ? '<span class="label label-success">上架</span>' : '<span class="label label-warning">下架</span>'
                    }
                },
                {
                    name: 'url', label: '海报', filter: function (item) {
                        if (item.url) {
                            return '<a target="_blank" href="' + item.url + '" title="点击预览"><img style="height:60px;border-radius: 5px" src="' + item.url + '"></a>'
                        } else {
                            return getDefaultImgStr();
                        }
                    }
                },
            ],
        },
    };
    diff = $.extend({}, diff, moreConstFn($search, search) || {});
    console.log(diff);
    if ($search.type) {
        arg = diff[$search.type];
        if (!arg) {
            if ($search.type.indexOf('category') > -1) {
                arg = diff['wild-category'];
            } else if ($search.type.indexOf('swiper') > -1) {
                arg = diff['wild-swiper'];
            } else if ($search.type.indexOf('article') > -1) {
                arg = diff['wild-article'];
            } else {
                return $('.table').html('<p style="text-align: center;line-height: 150px;font-size: 16px;">请寻求管理员协助打开此界面！</p>');
            }
        }
    }

    window.btns = arg.btns.map(function (item) { // 设置分类
        var btn = genBtn(item);
        if (item.onClick) onClick(btn, item.onClick)
        return btn;
    });
    setBtn(btns);

    function search(obj, isForce, isExport) {
        arg.destroyBtn();
        return gSearch({
            table: '.table',
            url: arg.query,
            query: obj,
            isForce: isForce,
            isExport: isExport,
            render: function (res) {
                var _$table = $('.table-responsive .table');
                destroyBtn();
                if(!!arg.beforeRender)res = arg.beforeRender(res);
                _$table.$table($.extend(TABLE_SETTING, {
                    checkbox: true, // 是否显示复选框
                    seqNum: true, // 是否显示序号
                    sort: search, //排序回调函数
                    destroyBtn: arg.destroyBtn, //禁用按钮
                    createBtn: arg.createBtn, // 启用她扭
                    toolbar: '.toolbar',
                    enableSetting: true,//允许自定义显示列
                    search: search,
                    showSearchComplex: true,
                    rendered: arg.rendered || function () {
                    },
                    fields: arg.fields,
                    data: arg.listFilter ? arg.listFilter(res) : res || [],
                }, arg.dateSearch ? { // 允许日期区间搜索
                    complexItems: [
                        {label: "", name: 'date', dateRange: true},
                    ]
                } : {}));
            }
        }, obj.first ? false : true);
    }

    search({}, true);
})


//# sourceMappingURL=../../../sys/scripts/sys/category/index.js.map
