/**
 * Created by suweiming on 2019/7/11.
 */
$(function () {
    var query = {},
        $update = $('.btn-update'),
        $delete = $('.btn-delete'),
        $create = $('.btn-create'),
        $ups = $('.batch-up'),
        $group = $('.batch-group'),
        $log = $('.btn-log'),
        //$upload = $('.btn-chongdi'),
        $banners = $('.btn-allow'),
        $plus = $('.plus'),
        $refuse = $('.btn-refuse'),
        $detail = $('.btn-dist-role'),
        $import = $('.btn-chongdi'),
        $tpl = $('.btn-hexiao'),
        strHtml = $('#form-template').html();

    var $search = parseSearch();
    if($search.rmBtn){
        $search.rmBtn.split(',').map(function(btn){
            if(btn=='add'){$create.remove();}
            if(btn=='rm'){$delete.remove();}
        })
    }

    commission({})

    function commission(obj) {
        gSearch({
            url: obj.url || '/',
            query: {},
            table: true,
            render: function (list) {

                var _$table = $('.table-responsive .table');
                return _$table.$table($.extend(TABLE_SETTING, {
                    checkbox: false, // 是否显示复选框
                    seqNum: false, // 是否显示序号
                    sort: commission, //排序回调函数
                    destroyBtn: destroyBtn, //禁用按钮
                    createBtn: createBtn, // 启用按钮
                    toolbar: '.content',
                    enableSetting: true,//允许自定义显示列
                    search: function (obj) { //搜索框查询函数
                        commission(obj)
                    },
                    rendered: function () {

                    },
                    btns: [
                        {
                            label: '修改',
                            class_: 'update',
                            onClick: function () {
                                var $json = $(this).data('json');
                                console.log($json);
                                var $index = dialogForm(
                                    {
                                        title: "修改",
                                        data: [
                                            {
                                                type: 'text',
                                                label: '模块名称',
                                                val: $json.name,
                                                require: true, // 是否必填
                                                placeholder: '请输入模块名称',
                                                name: 'name'
                                            },
                                            {
                                                type: 'textarea',
                                                label: '功能说明',
                                                val: $json.desc1,
                                                require: true, // 是否必填
                                                placeholder: '请输入功能说明',
                                                name: 'desc1'
                                            },

                                            {
                                                type: 'text',
                                                label: '模块类型',
                                                val: $json.orderNum,
                                                placeholder: '请输入模块类型',
                                                name: 'desc2'
                                            },
                                        ],
                                        submit: function (form) {
                                            form.id = $json.id; // 数据类型菜单
                                            form.type1 = $search.type; // 数据类型菜单
                                            $.post('/category/update', form, function (res) {
                                                if (res.code == 200) {
                                                    fnSuccess("操作成功");
                                                    search({}, true);
                                                    closeLayer($index);
                                                } else {
                                                    fnFail("操作失败");
                                                }
                                            })
                                        }
                                    });
                            }
                        },
                        {
                            label: '更多操作',
                            type: BUTTON_TYPE.DOWN_MENU,
                            items: [
                                {
                                    label: '通过',
                                    class_: 'import',
                                    onClick: function () {
                                        var $jon = $(this).data('json');
                                        var $json = {};
                                        var status = ($jon.isShow == '1' ? '0' : '1');
                                        var ti = ['targetId=' + $jon.id, 'type1=region', 'isShow=' + status];
                                        $.get('/statistic1/set?' + ti.join('&'), function (res) {
                                            if (res.code == 200) {
                                                commission({}, true);
                                            }
                                        })
                                    }
                                },
                                {
                                    label: '删除',
                                    class_: 'delete',
                                    onClick: function () {
                                        var $json = $(this).data('json');
                                        console.log($json);
                                        if (confirm('确认删除？')) {
                                            $.post('/category/delete/' + $json.id, {}, function (res) {
                                                if (res.code == 200) {
                                                    search({}, true);
                                                    fnSuccess('删除成功！');
                                                }
                                            })
                                        }
                                    }
                                },
                                {
                                    label: '禁用',
                                    class_: 'disabled',
                                    onClick: function () {
                                        var $jon = $(this).data('json');
                                        var $json = {};
                                        var status = ($jon.isShow == '1' ? '0' : '1');
                                        var ti = ['targetId=' + $jon.id, 'type1=region', 'isShow=' + status];
                                        $.get('/statistic1/set?' + ti.join('&'), function (res) {
                                            if (res.code == 200) {
                                                commission({}, true);
                                            }
                                        })
                                    }
                                },
                            ],
                        },
                    ],
                    destroyBtn: function () {
                        disableBtn(btns[0]);
                        disableBtn(btns[1]);
                    },
                    createBtn: function (_this) {
                        var $item = $(_this).data('json');
                        enableBtn(btns[0], $item);
                        enableBtn(btns[1], $item);
                    },
                    fields: [
                        {name: 'id', label: '模块ID',},
                        {name: 'title', label: '模块名称', enableSearch: true,},
                        {
                            name: 'handle', label: '功能说明', filter: function (item) {
                                return item.handle;
                            }
                        },
                        {
                            name: 'remark', label: '模块类型', filter: function (item) {
                                return item.remark;
                            }
                        },
                    ],
                    data: list || [],
                }));
            }
        })
    }
})
//# sourceMappingURL=../../../sys/scripts/sys/commission/modular.js.map
